/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GLVIEWER_H
#define GLVIEWER_H

#include <memory>
#include <mutex>

#include <nanogui/glutil.h>
#include <nanogui/screen.h>
#include <opencv2/core.hpp>

namespace slam
{

class Map;
class MapPoint;
class Tracking;
class System;
class Plane;

class GLTexture
{
  public:
  typedef std::unique_ptr<uint8_t[], void (*)(void *)> HandleType;

  GLTexture() = default;

  GLTexture(const std::string &textureName);
  GLTexture(const std::string &textureName, GLuint textureId);

  GLTexture(const GLTexture &other) = delete;
  GLTexture(GLTexture &&other) noexcept;

  GLTexture &operator=(const GLTexture &other) = delete;
  GLTexture &operator=(GLTexture &&other) noexcept;
  ~GLTexture() noexcept;

  GLuint texture() const { return mTextureId; }
  const std::string &textureName() const { return mTextureName; }

  /**
    *  Load a file in memory and create an OpenGL texture.
    *  Returns a handle type (an std::unique_ptr) to the loaded pixels.
    */
  HandleType load(const std::string &fileName);
  void load(cv::Mat &image);
  void update(cv::Mat &image);

  private:
  std::string mTextureName;
  GLuint mTextureId;
};


class Viewer : public nanogui::Screen
{
  public:
  Viewer(const std::string &title, System *system, Map *map);
  ~Viewer();

  void requestFinish();
  void setCurrentCameraPose(const cv::Mat &Tcw);
  void updateFrame(Tracking *pTracker);

  protected:
  virtual void drawContents();
  virtual void draw(NVGcontext *ctx);

  virtual bool resizeEvent(const Eigen::Vector2i &size);
  virtual bool keyboardEvent(int key, int scancode, int action, int modifiers);
  virtual bool mouseButtonEvent(const Eigen::Vector2i &p, int button, bool down, int modifiers);
  virtual bool mouseMotionEvent(const Eigen::Vector2i &p, const Eigen::Vector2i &rel, int button, int modifiers);
  virtual bool scrollEvent(const Eigen::Vector2i &p, const Eigen::Vector2f &rel);

  private:
  void loadSettings();
  void detectPlane();

  Eigen::Matrix4f getViewProjectionMatrix();

  void createMapShader();
  void createPlaneShader();
  void createKeyFrameShader();
  void createGraphShader();

  void drawMapPoints();
  void drawCamera();
  void drawKeyFrames();
  void drawGraph();
  void drawFrame();
  void drawFrameStats(cv::Mat &im, int nState);
  void drawPlane();

  std::mutex mMutexCamera;

  System *mpSystem;
  Map *mpMap;

  nanogui::GLShader mKFShader, mMapShader, mGraphShader, mPlaneShader;
  nanogui::Window *mFrameWindow;
  GLTexture mTex;
  cv::Mat mCameraPose, mTwc;
  Eigen::Matrix4f mProjMat, mViewMat;
  Plane *mPlane;

  bool mActive;
  float mZoom;
  Eigen::Vector2i mMousePosition;
  nanogui::Arcball mArcBall;
  Eigen::Vector2f mTranslation;
  Eigen::Vector3f mViewpoint;

  float mKeyFrameSize;
  float mKeyFrameLineWidth;
  float mGraphLineWidth;
  float mPointSize;
  float mCameraSize;
  float mCameraLineWidth;

  bool mbShowMap;
  bool mbShowKFs;
  bool mbShowGraph;
  bool mbFollowCamera;
  bool mbShowFrame;
  bool mbExitFinish;
  bool mbTranslate;

  // Current frame information
  cv::Size2f mTexSize;
  cv::Mat mTexture, mCurrentFrame;
  size_t N;
  std::vector<cv::KeyPoint> mvCurrentKeys;
  std::vector<bool> mvbMap, mvbVO;
  bool mbOnlyTracking;
  int mnTracked, mnTrackedVO;
  std::vector<cv::KeyPoint> mvIniKeys;
  std::vector<int> mvIniMatches;
  std::vector<MapPoint *> mvpMapPoints;
  int mState;
  size_t mnFID;
  std::mutex mMutexFrame;
};
}


#endif // VIEWER_H
