#ifndef TUMLOADER_H
#define TUMLOADER_H

#include "Loader.h"

namespace slam
{

class Viewer;

class TUMLoader : public Loader
{
  public:
  TUMLoader(const std::string &path, const std::shared_ptr<slam::System> &system);

  void LoadImages(
      const std::string &strFile,
      std::vector<std::string> &vstrImageFilenames,
      std::vector<double> &vTimestamps
  );

  void run();

private:
  std::string msPath;
};
}

#endif
