#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <list>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>

#define INFO_STREAM(l, x) std::cout << "\033[0;32m[" << l << "] " << x << "\033[0;0m" << std::endl;
#define WARN_STREAM(l, x) std::cout << "\033[0;33m[" << l << "] " << x << "\033[0;0m" << std::endl;
#define ERROR_STREAM(l, x) std::cerr << "\033[0;31m[" << l << "] " << x << "\033[0;0m" << std::endl;

#ifdef SLAM_DEBUG
#define DEBUG_STREAM(l, x) std::cout << "\033[0;35m[" << l << "] " << x << "\033[0;0m" << std::endl;
#else
#define DEBUG_STREAM(l, x)
#endif

#include <chrono> // Adapted from rosconsole. Copyright (c) 2008, Willow Garage, Inc.
#define WARN_STREAM_THROTTLE(rate, x)                                                                                   \
  do {                                                                                                                  \
    static double __log_stream_throttle__last_hit__ = 0.0;                                                              \
    std::chrono::time_point<std::chrono::system_clock> __log_stream_throttle__now__ = std::chrono::system_clock::now(); \
    if (__log_stream_throttle__last_hit__ + rate <= std::chrono::duration_cast<std::chrono::seconds>(                   \
                                                        __log_stream_throttle__now__.time_since_epoch())                \
                                                        .count()) {                                                     \
      __log_stream_throttle__last_hit__ = std::chrono::duration_cast<std::chrono::seconds>(                             \
          __log_stream_throttle__now__.time_since_epoch())                                                              \
                                              .count();                                                                 \
      WARN_STREAM(x);                                                                                                   \
    }                                                                                                                   \
  } while (0)

// Macros for performance monitoring
#ifdef SLAM_TRACE
#include <PerformanceMonitor.h>
extern slam::PerformanceMonitor *g_permon;
#define LOG(value) g_permon->log(std::string((#value)), (value))
#define LOG2(value1, value2) \
  LOG(value1);               \
  LOG(value2)
#define LOG3(value1, value2, value3) \
  LOG2(value1, value2);              \
  LOG(value3)
#define LOG4(value1, value2, value3, value4) \
  _LOG2(value1, value2);                     \
  LOG2(value3, value4)
#define START_TIMER(name) g_permon->startTimer((name))
#define STOP_TIMER(name) g_permon->stopTimer((name))
#define PRINT_TIMER(name) g_permon->printTime((name))
#else
#define LOG(v)
#define LOG2(v1, v2)
#define LOG3(v1, v2, v3)
#define LOG4(v1, v2, v3, v4)
#define START_TIMER(name)
#define STOP_TIMER(name)
#define PRINT_TIMER(name)
#endif

#endif
