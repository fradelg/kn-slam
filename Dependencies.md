## Dependencies

The source code heavily relies on the implementation of [ORB-SLAM2](https://github.com/raulmur/ORB_SLAM2), licensed under the GPLv3 terms.

### Code in **src** and **include** folders

* *ORBextractor.cc*.

This is a modified version of orb.cpp of OpenCV library. The original code is BSD licensed.

* *PnPsolver.h, PnPsolver.cc*.

This is a modified version of the epnp.h and epnp.cc of Vincent Lepetit, which can be found in popular BSD-licensed computer vision libraries as [OpenCV](https://github.com/Itseez/opencv/blob/master/modules/calib3d/src/epnp.cpp) and [OpenGV](https://github.com/laurentkneip/opengv/blob/master/src/absolute_pose/modules/Epnp.cpp). The original code is licensed under FreeBSD.

* Function *ORBmatcher::DescriptorDistance* in *ORBmatcher.cc*.

The code has been taken from [Sean Eron Anderson](http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel), where it is provided as public domain.

### Code in Thirdparty folder

#### DBoW2

This is a modified version of [DBoW2](https://github.com/dorian3d/DBoW2) and [DLib](https://github.com/dorian3d/DLib) library - BSD licensed.

#### g2o

This is a modified version of [g2o](https://github.com/RainerKuemmerle/g2o) - BSD licensed.

#### nanogui

Minimalistic GUI library for OpenGL 3.0 in C++11 [(see the original repository)](https://github.com/wjakob/nanogui) - BSD licensed.

### Other libraries

- [OpenCV 3](https://github.com/opencv/opencv) - BSD licensed.
- [Eigen 3](http://eigen.tuxfamily.org) - MPL2 licensed.