## KN-SLAM

KN-SLAM is a monocular SLAM system based on [ORB-SLAM](https://github.com/raulmur/ORB_SLAM2) with the focus on simplifying the connectivity graph.

## Features

- Load the Bag of Words (BoW) dictionary using the protobuf specification from Google.
- Perform an exhaustive search of correspondences where the 2D keypoint is linked to the best 3D point.
- Improve bootstrapping thanks to an adaptive approach that considers the number of retries.
- Reduce the number of false negative loops detected before loop closing.
- Constrain the maximum number of neighboring keyframes in the connectivity graph.

## Dependencies

- [OpenCV 3](https://github.com/opencv/opencv)
- [Eigen 3](https://eigen.tuxfamily.org/dox/)
- [protobuf](https://github.com/google/protobuf)
- [gLog](https://github.com/google/glog)

## Build

You can build the source code by cloning this repo:

```
git clone https://gitlab.com/fradelg/kn-slam.git
```

Then, the project can be built using CMake:

```
mkdir release
cd release
cmake ..
make
```

Finally, this will generate two executables: `mono_tum` and `mono_live`. The first one reads a set of images in the TUM RGB-D format and a set of settings from a YAML file. Then it tracks the position of the camera frame by frame. The second one is used for running SLAM over a video stream in real-time. The device number in OpenCV is used to identify the source of the video stream.

Next lines illustrate the CLI of both executables:

```
./release/mono_tum <path/to/orb-voc.proto> <path/to/settings.yml> <path/to/tum-rgbd/sequence> <show-gui>
./release/mono_live <path/to/orb-voc.proto> <path/to/settings.yml> <device-number> <show-gui>
```

There are examples of settings in the [tum-toolbox](https://gitlab.com/fradelg/tum-toolbox) repository, following a format similar to ORB-SLAM. Dataset can be downloaded from original sources and converted with the tools in the previously mentioned repository. If you want to inspect the already-converted datasets check this [link](http://fradelg.mobivap.es/datasets).

The `<show-gui>` parameter should be ommited to run SLAM without the GUI (useful for testing purposes).

## Notice

The original source code of ORB-SLAM has been modified but the original copyright notice has been maintained at the beginning of each file.