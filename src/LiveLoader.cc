#include <chrono>
#include <fstream>
#include <iostream>
#include <thread>

#include <opencv2/highgui.hpp>

#include <GLViewer.h>
#include <LiveLoader.h>
#include <Settings.h>
#include <global.h>

namespace slam
{

LiveLoader::LiveLoader(int index, const std::shared_ptr<System> &system)
    : Loader(system)
    , mCameraIndex(index)
{
}

void LiveLoader::run()
{
  cv::VideoCapture cap(mCameraIndex);
  if (!cap.isOpened())
    return;

  // Vector for tracking time statistics
  ulong nImages = 0;
  std::vector<double> vTimesTrack;

  // Time period accoding to camera framerate
  int fps = Settings::getInstance()["Camera.fps"];
  float cT = 1000.0 / fps;

  // Main loop
  for (;;) {
    // Read image from webcam
    cv::Mat im;
    cap >> im;

    auto t1 = std::chrono::steady_clock::now();

    // Pass the image to the SLAM system
    mpSystem->TrackMonocular(im, t1.time_since_epoch().count());

    auto t2 = std::chrono::steady_clock::now();
    double ttrack = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    vTimesTrack.push_back(ttrack);

    // Wait to load the next frame
    if (ttrack < cT) {
      long ms = static_cast<long>(cT - ttrack);
      std::this_thread::sleep_for(std::chrono::milliseconds(ms));
    }

    nImages++;

    if (stopRequested())
      break;
  }

  // Print some useful tracking stats
  std::sort(vTimesTrack.begin(), vTimesTrack.end());
  double totalTime = cv::sum(vTimesTrack)[0];
  INFO_STREAM("SUMMARY", "images: " << nImages)
  INFO_STREAM("SUMMARY", "total: " << totalTime)
  INFO_STREAM("SUMMARY", "median: " << vTimesTrack[vTimesTrack.size() / 2])
  INFO_STREAM("SUMMARY", "mean: " << totalTime / vTimesTrack.size())

  // Notify exit to viewer
  if (mpViewer)
    mpViewer->requestFinish();
}
}
