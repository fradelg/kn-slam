/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Converter.h>
#include <KeyFrame.h>
#include <ORBmatcher.h>
#include <global.h>

#define BEST_K_EDGES

namespace slam
{

uint64_t KeyFrame::nNextId = 0;
uint32_t KeyFrame::nMinObs = 15;
uint32_t KeyFrame::nMaxKFs = 20;

KeyFrame::KeyFrame(Frame &F, Map *pMap, KeyFrameDatabase *pKFDB)
    : mnFrameId(F.mnId)
    , mTimeStamp(F.mTimeStamp)
    , mnGridCols(FRAME_GRID_COLS)
    , mnGridRows(FRAME_GRID_ROWS)
    , mfGridElementWidthInv(F.mfGridElementWidthInv)
    , mfGridElementHeightInv(F.mfGridElementHeightInv)
    , mnTrackReferenceForFrame(0)
    , mnFuseTargetForKF(0)
    , mnBALocalForKF(0)
    , mnBAFixedForKF(0)
    , mnLoopQuery(0)
    , mnLoopWords(0)
    , mnRelocQuery(0)
    , mnRelocWords(0)
    , mnBAGlobalForKF(0)
    , fx(F.fx)
    , fy(F.fy)
    , cx(F.cx)
    , cy(F.cy)
    , invfx(F.invfx)
    , invfy(F.invfy)
    , mbf(F.mbf)
    , mb(F.mb)
    , mThDepth(F.mThDepth)
    , N(F.N)
    , mvKeys(F.mvKeys)
    , mvKeysUn(F.mvKeysUn)
    , mvuRight(F.mvuRight)
    , mvDepth(F.mvDepth)
    , mDescriptors(F.mDescriptors.clone())
    , mBowVec(F.mBowVec)
    , mFeatVec(F.mFeatVec)
    , mnScaleLevels(F.mnScaleLevels)
    , mfScaleFactor(F.mfScaleFactor)
    , mfLogScaleFactor(F.mfLogScaleFactor)
    , mvScaleFactors(F.mvScaleFactors)
    , mvLevelSigma2(F.mvLevelSigma2)
    , mvInvLevelSigma2(F.mvInvLevelSigma2)
    , mnMinX(F.mnMinX)
    , mnMinY(F.mnMinY)
    , mnMaxX(F.mnMaxX)
    , mnMaxY(F.mnMaxY)
    , mK(F.mK)
    , mImRGB(F.mImRGB)
    , mvpMapPoints(F.mvpMapPoints)
    , mpKeyFrameDB(pKFDB)
    , mpORBvocabulary(F.mpORBvocabulary)
    , mbFirstConnection(true)
    , mpParent(NULL)
    , mbNotErase(false)
    , mbToBeErased(false)
    , mbBad(false)
    , mHalfBaseline(F.mb / 2)
    , mpMap(pMap)
{
  mnId = nNextId++;

  mGrid.resize(mnGridCols);
  for (int i = 0; i < mnGridCols; i++) {
    mGrid[i].resize(mnGridRows);
    for (int j = 0; j < mnGridRows; j++)
      mGrid[i][j] = F.mGrid[i][j];
  }

  SetPose(F.mTcw);
}

void KeyFrame::ComputeBoW()
{
  if (mBowVec.empty() || mFeatVec.empty()) {
    std::vector<cv::Mat> vCurrentDesc = Converter::toDescriptorVector(mDescriptors);
    // Feature vector associate features with nodes in the 4th level (from leaves up)
    // We assume the vocabulary tree has 6 levels, change 4 otherwise
    mpORBvocabulary->transform(vCurrentDesc, mBowVec, mFeatVec, 4);
  }
}

void KeyFrame::SetPose(const cv::Mat &Tcw_)
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  Tcw_.copyTo(Tcw);
  cv::Mat Rcw = Tcw.rowRange(0, 3).colRange(0, 3);
  cv::Mat tcw = Tcw.rowRange(0, 3).col(3);
  cv::Mat Rwc = Rcw.t();
  Ow = -Rwc * tcw;

  Twc = cv::Mat::eye(4, 4, Tcw.type());
  Rwc.copyTo(Twc.rowRange(0, 3).colRange(0, 3));
  Ow.copyTo(Twc.rowRange(0, 3).col(3));
  cv::Mat center = (cv::Mat_<float>(4, 1) << mHalfBaseline, 0, 0, 1);
  Cw = Twc * center;
}

cv::Mat KeyFrame::GetPose()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Tcw.clone();
}

cv::Mat KeyFrame::GetPoseInverse()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Twc.clone();
}

cv::Mat KeyFrame::GetCameraCenter()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Ow.clone();
}

cv::Mat KeyFrame::GetStereoCenter()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Cw.clone();
}

cv::Mat KeyFrame::GetRotation()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Tcw.rowRange(0, 3).colRange(0, 3).clone();
}

cv::Mat KeyFrame::GetTranslation()
{
  std::unique_lock<std::mutex> lock(mMutexPose);
  return Tcw.rowRange(0, 3).col(3).clone();
}

void KeyFrame::AddConnection(KeyFrame *pKF, const unsigned &weight)
{
  {
    std::unique_lock<std::mutex> lock(mMutexConnections);
    if (mConnectedKeyFrameWeights[pKF] == weight)
      return;
    mConnectedKeyFrameWeights[pKF] = weight;
  }

  UpdateBestCovisibles();
}

void KeyFrame::UpdateBestCovisibles()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);

  // Convert STL map to STL vector of pairs for sorting
  std::vector<std::pair<KeyFrame *, uint32_t>> vPairs;
  vPairs.reserve(mConnectedKeyFrameWeights.size());
  uint32_t obs_total = 0;
  for (auto &obs : mConnectedKeyFrameWeights) {
    vPairs.push_back(obs);
    obs_total += obs.second;
  }

  std::sort(vPairs.begin(), vPairs.end(), [=](std::pair<KeyFrame *, unsigned> &a, std::pair<KeyFrame *, unsigned> &b) {
    return a.second > b.second;
  });

  mvOrderedWeights.clear();
  mvpOrderedConnectedKeyFrames.clear();

#ifdef BEST_K_EDGES
  uint32_t k_count = 0;
  uint32_t obs_count = 0;
  //mConnectedKeyFrameWeights.clear();
#endif

  for (auto &obs : vPairs) {
#ifdef BEST_K_EDGES
    k_count++;
    obs_count += obs.second;
    //mConnectedKeyFrameWeights.insert(obs);
    if (k_count > nMaxKFs || (k_count > 5 && obs_count > 0.95 * obs_total))
      break;
#endif
    mvOrderedWeights.push_back(obs.second);
    mvpOrderedConnectedKeyFrames.push_back(obs.first);
  }

  DEBUG_STREAM("UpdateBestCovisibles", mvOrderedWeights.size() << " / " << vPairs.size());
}

std::set<KeyFrame *> KeyFrame::GetConnectedKeyFrames()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
#ifdef BEST_K_EDGES
  return std::set<KeyFrame *>(mvpOrderedConnectedKeyFrames.begin(), mvpOrderedConnectedKeyFrames.end());
#else
  std::set<KeyFrame *> result;
  for (auto &obs : mConnectedKeyFrameWeights)
    result.insert(obs.first);
  return result;
#endif
}

std::vector<KeyFrame *> KeyFrame::GetCovisibleKeyFrames()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  return mvpOrderedConnectedKeyFrames;
}

std::vector<KeyFrame *> KeyFrame::GetBestCovisibilityKeyFrames(const size_t N)
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  if (mvpOrderedConnectedKeyFrames.size() < N)
    return mvpOrderedConnectedKeyFrames;
  else
    return std::vector<KeyFrame *>(mvpOrderedConnectedKeyFrames.begin(), mvpOrderedConnectedKeyFrames.begin() + N);
}

std::vector<KeyFrame *> KeyFrame::GetCovisiblesByWeight(const unsigned w)
{
  std::unique_lock<std::mutex> lock(mMutexConnections);

  if (mvpOrderedConnectedKeyFrames.empty())
    return std::vector<KeyFrame *>();

  auto it = std::upper_bound(mvOrderedWeights.begin(), mvOrderedWeights.end(), w, KeyFrame::weightComp);
  if (it == mvOrderedWeights.end())
    return std::vector<KeyFrame *>();
  else {
    long D = it - mvOrderedWeights.begin();
    return std::vector<KeyFrame *>(mvpOrderedConnectedKeyFrames.begin(), mvpOrderedConnectedKeyFrames.begin() + D);
  }
}

std::vector<unsigned> KeyFrame::GetCovisibilityWeights()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  return mvOrderedWeights;
}

unsigned KeyFrame::GetWeight(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  if (mConnectedKeyFrameWeights.count(pKF))
    return mConnectedKeyFrameWeights[pKF];
  else
    return 0;
}

void KeyFrame::AddMapPoint(MapPoint *pMP, const size_t &idx)
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);
  mvpMapPoints[idx] = pMP;
}

void KeyFrame::EraseMapPointMatch(const size_t &idx)
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);
  mvpMapPoints[idx] = NULL;
}

void KeyFrame::EraseMapPointMatch(MapPoint *pMP)
{
  int idx = pMP->GetIndexInKeyFrame(this);
  if (idx >= 0)
    mvpMapPoints[idx] = NULL;
}

void KeyFrame::ReplaceMapPointMatch(const size_t &idx, MapPoint *pMP)
{
  mvpMapPoints[idx] = pMP;
}

std::set<MapPoint *> KeyFrame::GetMapPoints()
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);
  std::set<MapPoint *> s;
  for (MapPoint *pMP : mvpMapPoints) {
    if (!pMP)
      continue;
    if (!pMP->isBad())
      s.insert(pMP);
  }
  return s;
}

size_t KeyFrame::TrackedMapPoints(const uint32_t &minObs)
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);

  size_t nPoints = 0;
  for (MapPoint *pMP : mvpMapPoints) {
    if (pMP) {
      if (!pMP->isBad()) {
        if (minObs > 0) {
          if (pMP->Observations() >= minObs)
            nPoints++;
        } else {
          nPoints++;
        }
      }
    }
  }

  return nPoints;
}

std::vector<MapPoint *> KeyFrame::GetMapPointMatches()
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);
  return mvpMapPoints;
}

MapPoint *KeyFrame::GetMapPoint(const size_t &idx)
{
  std::unique_lock<std::mutex> lock(mMutexFeatures);
  return mvpMapPoints[idx];
}

void KeyFrame::UpdateConnections()
{
  std::map<KeyFrame *, uint32_t> KFcounter;

  // Accounting the points shared with other KFs
  for (MapPoint *pMP : GetMapPointMatches()) {

    if (!pMP || pMP->isBad())
      continue;

    for (auto &obs : pMP->GetObservations()) {
      if (obs.first->mnId != this->mnId)
        KFcounter[obs.first]++;
    }
  }

  // This should not happen
  if (KFcounter.empty()) {
    std::cerr << "ERROR: keyframe " << mnId << " does not have neighbours" << std::endl;
    return;
  }

  std::pair<KeyFrame *, unsigned> bestKF(nullptr, 0);
  std::vector<std::pair<KeyFrame *, unsigned>> vPairs;
  vPairs.reserve(KFcounter.size());
  for (auto &obs : KFcounter) {

    if (obs.second > bestKF.second)
      bestKF = obs;

    if (obs.second > nMinObs)
      vPairs.push_back(obs);
  }

  if (vPairs.empty())
    vPairs.push_back(bestKF);

  std::sort(vPairs.begin(), vPairs.end(), [=](std::pair<KeyFrame *, unsigned> &a, std::pair<KeyFrame *, unsigned> &b) {
    return a.second > b.second;
  });

  uint32_t obs_total = 0;
  for (auto &obs : vPairs) {
    obs.first->AddConnection(this, obs.second);
    obs_total += obs.second;
  }

  // Update my connections with the K-best KF
  std::vector<KeyFrame *> lKFs;
  std::vector<unsigned> lWs;
#ifdef BEST_K_EDGES
  uint32_t k_count = 0;
  uint32_t obs_count = 0;
  //KFcounter.clear(); // Constraint the KF-weight table to KF sharing more than MAX_K observations
#endif

  for (auto &obs : vPairs) {
#ifdef BEST_K_EDGES
    k_count++;
    obs_count += obs.second;
    //KFcounter.insert(obs);
    //obs.first->AddConnection(this, obs.second);
    if (k_count > nMaxKFs || (k_count > 5 && obs_count > 0.95 * obs_total))
      break;
#endif
    lKFs.push_back(obs.first);
    lWs.push_back(obs.second);
  }

  DEBUG_STREAM("UpdateConnections", lKFs.size() << " / " << vPairs.size());

  {
    std::unique_lock<std::mutex> lockCon(mMutexConnections);

    mConnectedKeyFrameWeights = KFcounter;
    mvpOrderedConnectedKeyFrames = lKFs;
    mvOrderedWeights = lWs;

    // attach this KeyFrame to its parent
    if (mbFirstConnection && mnId != 0) {
      mbFirstConnection = false;
      mpParent = mvpOrderedConnectedKeyFrames.front();
      mpParent->AddChild(this);
    }
  }
}

void KeyFrame::AddChild(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  mspChildrens.insert(pKF);
}

void KeyFrame::EraseChild(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  mspChildrens.erase(pKF);
}

void KeyFrame::ChangeParent(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  mpParent = pKF;
  pKF->AddChild(this);
}

std::set<KeyFrame *> KeyFrame::GetChilds()
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  return mspChildrens;
}

KeyFrame *KeyFrame::GetParent()
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  return mpParent;
}

bool KeyFrame::hasChild(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  return mspChildrens.count(pKF);
}

void KeyFrame::AddLoopEdge(KeyFrame *pKF)
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  mbNotErase = true;
  mspLoopEdges.insert(pKF);
}

std::set<KeyFrame *> KeyFrame::GetLoopEdges()
{
  std::unique_lock<std::mutex> lockCon(mMutexConnections);
  return mspLoopEdges;
}

void KeyFrame::SetNotErase()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  mbNotErase = true;
}

void KeyFrame::SetErase()
{
  {
    std::unique_lock<std::mutex> lock(mMutexConnections);
    if (mspLoopEdges.empty()) {
      mbNotErase = false;
    }
  }

  if (mbToBeErased) {
    SetBadFlag();
  }
}

void KeyFrame::SetBadFlag()
{
  {
    std::unique_lock<std::mutex> lock(mMutexConnections);
    if (mnId == 0)
      return;
    else if (mbNotErase) {
      mbToBeErased = true;
      return;
    }
  }

  for (auto &pKi : mConnectedKeyFrameWeights)
    pKi.first->EraseConnection(this);

  for (size_t i = 0; i < mvpMapPoints.size(); i++)
    if (mvpMapPoints[i])
      mvpMapPoints[i]->EraseObservation(this);
  {
    std::unique_lock<std::mutex> lock(mMutexConnections);
    std::unique_lock<std::mutex> lock1(mMutexFeatures);

    mConnectedKeyFrameWeights.clear();
    mvpOrderedConnectedKeyFrames.clear();

    // Update Spanning Tree
    std::set<KeyFrame *> sParentCandidates;
    sParentCandidates.insert(mpParent);

    // Assign at each iteration one children with a parent (the pair with highest covisibility weight)
    // Include that child as new parent candidate for the rest
    while (!mspChildrens.empty()) {
      bool bContinue = false;

      int max = -1;
      KeyFrame *pC;
      KeyFrame *pP;

      for (KeyFrame *pKFchild : mspChildrens) {
        if (pKFchild->isBad())
          continue;

        // Check if a parent candidate is connected to the child keyframe
        for (KeyFrame *pKFcovisble : pKFchild->GetCovisibleKeyFrames()) {
          for (KeyFrame *pKFp : sParentCandidates) {
            if (pKFcovisble->mnId == pKFp->mnId) {
              int w = pKFchild->GetWeight(pKFcovisble);
              if (w > max) {
                pC = pKFchild;
                pP = pKFcovisble;
                max = w;
                bContinue = true;
              }
            }
          }
        }
      }

      if (bContinue) {
        pC->ChangeParent(pP);
        sParentCandidates.insert(pC);
        mspChildrens.erase(pC);
      } else
        break;
    }

    // If a child has no covisibility links with any parent candidate, assign to my original parent
    if (!mspChildrens.empty())
      for (KeyFrame *pKFc : mspChildrens) {
        pKFc->ChangeParent(mpParent);
      }

    mpParent->EraseChild(this);
    mTcp = Tcw * mpParent->GetPoseInverse();
    mbBad = true;
  }

  mpMap->EraseKeyFrame(this);
  mpKeyFrameDB->erase(this);
}

bool KeyFrame::isBad()
{
  std::unique_lock<std::mutex> lock(mMutexConnections);
  return mbBad;
}

void KeyFrame::EraseConnection(KeyFrame *pKF)
{
  bool bUpdate = false;
  {
    std::unique_lock<std::mutex> lock(mMutexConnections);
    if (mConnectedKeyFrameWeights.count(pKF)) {
      mConnectedKeyFrameWeights.erase(pKF);
      bUpdate = true;
    }
  }

  if (bUpdate)
    UpdateBestCovisibles();
}

std::vector<size_t> KeyFrame::GetFeaturesInArea(const float &x, const float &y, const float &r) const
{
  std::vector<size_t> vIndices;
  vIndices.reserve(N);

  const int nMinCellX = std::max(0, (int)floor((x - mnMinX - r) * mfGridElementWidthInv));
  if (nMinCellX >= mnGridCols)
    return vIndices;

  const int nMaxCellX = std::min((int)mnGridCols - 1, (int)ceil((x - mnMinX + r) * mfGridElementWidthInv));
  if (nMaxCellX < 0)
    return vIndices;

  const int nMinCellY = std::max(0, (int)floor((y - mnMinY - r) * mfGridElementHeightInv));
  if (nMinCellY >= mnGridRows)
    return vIndices;

  const int nMaxCellY = std::min((int)mnGridRows - 1, (int)ceil((y - mnMinY + r) * mfGridElementHeightInv));
  if (nMaxCellY < 0)
    return vIndices;

  for (int ix = nMinCellX; ix <= nMaxCellX; ix++) {
    for (int iy = nMinCellY; iy <= nMaxCellY; iy++) {
      const std::vector<size_t> vCell = mGrid[ix][iy];
      for (size_t j = 0, jend = vCell.size(); j < jend; j++) {
        const cv::KeyPoint &kpUn = mvKeysUn[vCell[j]];
        const float distx = kpUn.pt.x - x;
        const float disty = kpUn.pt.y - y;

        if (fabs(distx) < r && fabs(disty) < r)
          vIndices.push_back(vCell[j]);
      }
    }
  }

  return vIndices;
}

bool KeyFrame::IsInImage(const float &x, const float &y) const
{
  return (x >= mnMinX && x < mnMaxX && y >= mnMinY && y < mnMaxY);
}

cv::Vec3b KeyFrame::GetPixel(const size_t &idx)
{
  cv::KeyPoint kp = this->mvKeys[idx];
  return mImRGB.at<cv::Vec3b>(kp.pt.y, kp.pt.x);
}

cv::Mat KeyFrame::UnprojectStereo(int i)
{
  const float z = mvDepth[i];
  if (z > 0) {
    const float u = mvKeys[i].pt.x;
    const float v = mvKeys[i].pt.y;
    const float x = (u - cx) * z * invfx;
    const float y = (v - cy) * z * invfy;
    cv::Mat x3Dc = (cv::Mat_<float>(3, 1) << x, y, z);

    std::unique_lock<std::mutex> lock(mMutexPose);
    return Twc.rowRange(0, 3).colRange(0, 3) * x3Dc + Twc.rowRange(0, 3).col(3);
  } else
    return cv::Mat();
}

float KeyFrame::ComputeSceneMedianDepth(const uint32_t q)
{
  std::vector<MapPoint *> vpMapPoints;
  cv::Mat Tcw_;
  {
    std::unique_lock<std::mutex> lock(mMutexFeatures);
    std::unique_lock<std::mutex> lock2(mMutexPose);
    vpMapPoints = mvpMapPoints;
    Tcw_ = Tcw.clone();
  }

  std::vector<float> vDepths;
  vDepths.reserve(N);
  cv::Mat Rcw2 = Tcw_.row(2).colRange(0, 3);
  Rcw2 = Rcw2.t();
  float zcw = Tcw_.at<float>(2, 3);
  for (int i = 0; i < N; i++) {
    if (mvpMapPoints[i]) {
      MapPoint *pMP = mvpMapPoints[i];
      cv::Mat x3Dw = pMP->GetWorldPos();
      float z = Rcw2.dot(x3Dw) + zcw;
      vDepths.push_back(z);
    }
  }

  std::sort(vDepths.begin(), vDepths.end());

  return vDepths[(vDepths.size() - 1) / q];
}

} // namespace slam
