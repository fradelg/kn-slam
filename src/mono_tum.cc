/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <unistd.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <FrameDrawer.h>
#include <GLViewer.h>
#include <System.h>
#include <TUMLoader.h>
#include <Tracking.h>
#include <global.h>

int main(int argc, char **argv)
{
  if (argc < 4) {
    ERROR_STREAM("Usage", "mono_tum path_to_vocabulary path_to_settings path_to_sequence [viewer]")
    ERROR_STREAM("Example", "mono_tum data/orb-voc.proto data/tum1.yml /data/bags/tum/fr1_xyz")
    return 1;
  }

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  auto slam = std::make_shared<slam::System>(argv[1], argv[2], slam::System::MONOCULAR);
  auto loader = std::make_shared<slam::TUMLoader>(argv[3], slam);
  std::thread loadert(&slam::TUMLoader::run, loader);

  if (argc == 5) {
    try {
      // GUI main loop (glfw functions must be called in the main thread)
      nanogui::init();

      // Create GLViewer only after nanogui initialization
      slam::Viewer *viewer = new slam::Viewer(argv[3], slam.get(), slam->getMap());
      viewer->drawAll();
      viewer->setVisible(true);

      // Automatically close viewer when loader consumes all frames
      loader->setViewer(viewer);
      // Link tracker with viewer to update camera pose and image
      slam->getTracker()->SetViewer(viewer);

      // Start GUI event processing loop
      nanogui::mainloop();

      // Exit gracefully
      nanogui::shutdown();
      loader->stop();
    } catch (const std::runtime_error &e) {
      std::cerr << e.what() << std::endl;
    }
  }

  loadert.join();
  slam->Shutdown();

  INFO_STREAM("SYSTEM", "# 1st frame: " << slam->getTracker()->mInitialFrame.mnId)
  INFO_STREAM("SYSTEM", "# Frames: " << slam->getTracker()->mCurrentFrame.mnId)

  // Save keyframe trajectory, histogram and map
  slam->SaveKeyFrameTrajectoryTUM("/tmp/trajectory.txt");
  slam->SaveEdgeHistogram("/tmp/histogram.txt");
  slam->SaveMap("/tmp/map.ply");
  slam->SaveGraphDOT("/tmp/poses.gv");

  // Save timers and logs
#ifdef SLAM_TRACE
  g_permon->writeToFile();
#endif

  return 0;
}
