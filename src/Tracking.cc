/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>

#include <Converter.h>
#include <FrameDrawer.h>
#include <GLViewer.h>
#include <Initializer.h>
#include <Map.h>
#include <ORBmatcher.h>
#include <Optimizer.h>
#include <PnPsolver.h>
#include <Settings.h>
#include <SparseImgAlign.h>
#include <System.h>
#include <Tracking.h>
#include <global.h>

namespace slam
{

Tracking::Tracking(System *pSys,
    ORBVocabulary *pVoc,
    Map *pMap,
    KeyFrameDatabase *pKFDB,
    const int sensor)
    : mState(NO_IMAGES_YET)
    , mSensor(sensor)
    , mbOnlyTracking(false)
    , mbVO(false)
    , mpORBVocabulary(pVoc)
    , mpKeyFrameDB(pKFDB)
    , mpInitializer(NULL)
    , mpSystem(pSys)
    , mpViewer(NULL)
    , mpMap(pMap)
    , mnLastRelocFrameId(0)
    , mnMinMatches(100)
    , mnInitFails(0)
{
  LoadCalibration();

  cv::FileNode node = Settings::getInstance()["KeyFrame"]["minObs"];
  if (!node.empty())
    KeyFrame::nMinObs = static_cast<int32_t>(node);

  node = Settings::getInstance()["KeyFrame"]["maxKFs"];
  if (!node.empty())
    KeyFrame::nMaxKFs = static_cast<int32_t>(node);

  int fps = Settings::getInstance().get("Camera.fps");
  if (fps == 0)
    fps = 30;

  // Max/Min Frames to insert keyframes and to check relocalisation
  mMinFrames = 0;
  mMaxFrames = fps;

  int nRGB = Settings::getInstance().get("Camera.RGB");
  mbRGB = nRGB;

  // Load ORB parameters
  int nFeatures = Settings::getInstance().get("ORBextractor.nFeatures");
  int nLevels = Settings::getInstance().get("ORBextractor.nLevels");
  int fIniThFAST = Settings::getInstance().get("ORBextractor.iniThFAST");
  int fMinThFAST = Settings::getInstance().get("ORBextractor.minThFAST");
  float fScaleFactor = Settings::getInstance().get("ORBextractor.scaleFactor");

  mpORBextractorLeft = new ORBextractor(nFeatures, fScaleFactor, nLevels, fIniThFAST, fMinThFAST);

  if (sensor == System::STEREO)
    mpORBextractorRight = new ORBextractor(nFeatures, fScaleFactor, nLevels, fIniThFAST, fMinThFAST);

  if (sensor == System::MONOCULAR)
    mpIniORBextractor = new ORBextractor(2 * nFeatures, fScaleFactor, nLevels, fIniThFAST, fMinThFAST);

  if (sensor == System::STEREO || sensor == System::RGBD) {
    mThDepth = mbf * static_cast<float>(Settings::getInstance().get("ThDepth")) / mK.at<float>(0, 0);
  }

  if (sensor == System::RGBD) {
    mDepthMapFactor = Settings::getInstance().get("DepthMapFactor");
    if (fabs(mDepthMapFactor) < 1e-5)
      mDepthMapFactor = 1;
    else
      mDepthMapFactor = 1.0f / mDepthMapFactor;
  }
}

void Tracking::SetLocalMapper(LocalMapping *pLocalMapper)
{
  mpLocalMapper = pLocalMapper;
}

void Tracking::SetLoopClosing(LoopClosing *pLoopClosing)
{
  mpLoopClosing = pLoopClosing;
}

void Tracking::SetViewer(Viewer *pViewer)
{
  mpViewer = pViewer;
}

cv::Mat Tracking::GrabImageStereo(const cv::Mat &imRectLeft, const cv::Mat &imRectRight, const double &timestamp)
{
  mCurrentFrame = Frame(imRectLeft, imRectRight, timestamp, mpORBextractorLeft, mpORBextractorRight, mpORBVocabulary, mK, mDistCoef, mbf, mThDepth);

  Track();

  return mCurrentFrame.mTcw.clone();
}


cv::Mat Tracking::GrabImageRGBD(const cv::Mat &imRGB, const cv::Mat &imD, const double &timestamp)
{
  cv::Mat imDepth = imD;
  if ((std::fabs(mDepthMapFactor - 1.0f) > 1e-5) || imDepth.type() != CV_32F)
    imDepth.convertTo(imDepth, CV_32F, mDepthMapFactor);

  mCurrentFrame = Frame(imRGB, imDepth, timestamp, mpORBextractorLeft, mpORBVocabulary, mK, mDistCoef, mbf, mThDepth);

  Track();

  return mCurrentFrame.mTcw.clone();
}


cv::Mat Tracking::GrabImageMonocular(const cv::Mat &im, const double &timestamp)
{
  ORBextractor *ext = (mState == NOT_INITIALIZED || mState == NO_IMAGES_YET) ? mpIniORBextractor : mpORBextractorLeft;
  mCurrentFrame = Frame(im, timestamp, ext, mpORBVocabulary, mK, mDistCoef, mbf, mThDepth);

  Track();

  return mCurrentFrame.mTcw.clone();
}

void Tracking::Track()
{
  if (mState == NO_IMAGES_YET)
    mState = NOT_INITIALIZED;

  mLastProcessedState = mState;

  // Get Map Mutex -> Map cannot be changed
  std::unique_lock<std::mutex> lock(mpMap->mMutexMapUpdate);

  if (mState == NOT_INITIALIZED) {
    if (mSensor == System::STEREO || mSensor == System::RGBD)
      StereoInitialization();
    else
      MonocularInitialization();

    if (mpViewer)
      mpViewer->updateFrame(this);

    if (mState != OK)
      return;
  } else {
    // System is initialized. Track Frame.
    bool bOK = true;

    // Initial camera pose estimation using motion model or relocalization (if tracking is lost)
    if (!mbOnlyTracking) {

      // Local Mapping is activated
      if (mState == OK) {
        // Local Mapping might have changed some MapPoints tracked in last frame
        CheckReplacedInLastFrame();

        if (mVelocity.empty() || mCurrentFrame.mnId < mnLastRelocFrameId + 3) { // @fradelg: 3
          bOK = TrackReferenceKeyFrame();
        } else {
          if (!TrackWithMotionModel()) {
            bOK = TrackReferenceKeyFrame();
          }
        }
      } else {
        bOK = Relocalization();
      }
    } else {
      // Localization Mode: Local Mapping is deactivated
      if (mState == LOST) {
        bOK = Relocalization();
      } else {
        if (!mbVO) {
          // In last frame we tracked enough MapPoints in the map
          if (!mVelocity.empty()) {
            bOK = TrackWithMotionModel();
          } else {
            bOK = TrackReferenceKeyFrame();
          }
        } else {
          // In last frame we tracked mainly "visual odometry" points.

          // We compute two camera poses, one from motion model and one doing relocalization.
          // If relocalization is sucessfull we choose that solution, otherwise we retain
          // the "visual odometry" solution.

          bool bOKMM = false;
          bool bOKReloc = false;
          std::vector<MapPoint *> vpMPsMM;
          std::vector<bool> vbOutMM;
          cv::Mat TcwMM;

          if (!mVelocity.empty()) {
            bOKMM = TrackWithMotionModel();
            vpMPsMM = mCurrentFrame.mvpMapPoints;
            vbOutMM = mCurrentFrame.mvbOutlier;
            TcwMM = mCurrentFrame.mTcw.clone();
          }

          bOKReloc = Relocalization();
          if (bOKMM && !bOKReloc) {
            mCurrentFrame.SetPose(TcwMM);
            mCurrentFrame.mvpMapPoints = vpMPsMM;
            mCurrentFrame.mvbOutlier = vbOutMM;

            if (mbVO) {
              for (size_t i = 0; i < mCurrentFrame.N; i++) {
                if (mCurrentFrame.mvpMapPoints[i] && !mCurrentFrame.mvbOutlier[i]) {
                  mCurrentFrame.mvpMapPoints[i]->IncreaseFound();
                }
              }
            }

          } else if (bOKReloc) {
            mbVO = false;
          }

          bOK = bOKReloc || bOKMM;
        }
      }
    }

    mCurrentFrame.mpReferenceKF = mpReferenceKF;

    if (!mbOnlyTracking) {
      if (bOK)
        // We have an estimated camera pose and matching -> Track the local map
        bOK = TrackLocalMap();
      // Try to perform a direct sparse image alignment using the projection of the keypoints
      //      if (!bOK) {
      //        SparseImgAlign img_align(4, 2, 30, SparseImgAlign::GaussNewton, false, false);
      //        size_t n_tracked = img_align.run(&mLastFrame, &mCurrentFrame);
      //        std::cout << "[# Direct Tracked Features] = " << n_tracked << std::endl;
      //        bOK = (n_tracked > 20);
      //      }
    } else if (bOK && !mbVO) {
      // mbVO true means that there are few matches to MapPoints in the map. We cannot retrieve
      // a local map and therefore we do not perform TrackLocalMap(). Once the system relocalizes
      // the camera we will use the local map again.
      bOK = TrackLocalMap();
    }

    mState = bOK ? OK : LOST;

    if (mpViewer)
      mpViewer->updateFrame(this);

    // If tracking were good, check if we need a new keyframe
    if (bOK) {
      // Update motion model
      if (!mLastFrame.mTcw.empty()) {
        cv::Mat LastTwc = cv::Mat::eye(4, 4, CV_32F);
        mLastFrame.GetRotationInverse().copyTo(LastTwc.rowRange(0, 3).colRange(0, 3));
        mLastFrame.GetCameraCenter().copyTo(LastTwc.rowRange(0, 3).col(3));
        mVelocity = mCurrentFrame.mTcw * LastTwc;
      } else
        mVelocity = cv::Mat();

      if (mpViewer)
        mpViewer->setCurrentCameraPose(mCurrentFrame.mTcw);

      // Clean VO matches
      for (size_t i = 0; i < mCurrentFrame.N; i++) {
        MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
        if (pMP)
          if (pMP->Observations() < 1) {
            mCurrentFrame.mvbOutlier[i] = false;
            mCurrentFrame.mvpMapPoints[i] = NULL;
          }
      }

      // Delete temporal MapPoints
      for (MapPoint *pMP : mlpTemporalPoints)
        delete pMP;
      mlpTemporalPoints.clear();

      // Check if we need to insert a new keyframe
      if (NeedNewKeyFrame())
        CreateNewKeyFrame();

      // We allow points with high innovation (considererd outliers by the Huber Function)
      // pass to the new keyframe, so that bundle adjustment will finally decide
      // if they are outliers or not. We don't want to estimate the position of next frame
      // with those points so we discard them.
      for (size_t i = 0; i < mCurrentFrame.N; i++) {
        if (mCurrentFrame.mvpMapPoints[i] && mCurrentFrame.mvbOutlier[i])
          mCurrentFrame.mvpMapPoints[i] = NULL;
      }
    }

    // Reset if the camera get lost soon after initialization
    if (mState == LOST) {
      if (mpMap->KeyFramesInMap() <= 5) {
        WARN_STREAM("TRACKING", "Track lost soon after initialization. Reset")
        mpSystem->Reset();
        return;
      }
    }

    if (!mCurrentFrame.mpReferenceKF)
      mCurrentFrame.mpReferenceKF = mpReferenceKF;

    mLastFrame = Frame(mCurrentFrame);
  }

  // Store frame pose information to retrieve the complete camera trajectory afterwards.
  if (!mCurrentFrame.mTcw.empty()) {
    cv::Mat Tcr = mCurrentFrame.mTcw * mCurrentFrame.mpReferenceKF->GetPoseInverse();
    mlRelativeFramePoses.push_back(Tcr);
    mlpReferences.push_back(mpReferenceKF);
    mlFrameTimes.push_back(mCurrentFrame.mTimeStamp);
    mlbLost.push_back(mState == LOST);
  } else {
    // This can happen if tracking is lost
    mlRelativeFramePoses.push_back(mlRelativeFramePoses.back());
    mlpReferences.push_back(mlpReferences.back());
    mlFrameTimes.push_back(mlFrameTimes.back());
    mlbLost.push_back(mState == LOST);
  }
}


void Tracking::StereoInitialization()
{
  if (mCurrentFrame.N > 500) {
    // Set Frame pose to the origin
    mCurrentFrame.SetPose(cv::Mat::eye(4, 4, CV_32F));

    // Create KeyFrame
    KeyFrame *pKFini = new KeyFrame(mCurrentFrame, mpMap, mpKeyFrameDB);

    // Insert KeyFrame in the map
    mpMap->AddKeyFrame(pKFini);

    // Create MapPoints and link to KeyFrame
    for (size_t i = 0; i < mCurrentFrame.N; i++) {
      float z = mCurrentFrame.mvDepth[i];
      if (z > 0) {
        cv::Mat x3D = mCurrentFrame.UnprojectStereo(i);
        MapPoint *pNewMP = new MapPoint(x3D, pKFini, mpMap);
        pNewMP->AddObservation(pKFini, i);
        pKFini->AddMapPoint(pNewMP, i);
        pNewMP->ComputeDistinctiveDescriptors();
        pNewMP->UpdateNormalAndDepth();
        mpMap->AddMapPoint(pNewMP);

        mCurrentFrame.mvpMapPoints[i] = pNewMP;
      }
    }

    INFO_STREAM("MAPPING", "New map created with " << mpMap->PointsCount() << " points")

    mpLocalMapper->InsertKeyFrame(pKFini);

    mLastFrame = Frame(mCurrentFrame);
    mnLastKeyFrameId = mCurrentFrame.mnId;
    mpLastKeyFrame = pKFini;

    mvpLocalKeyFrames.push_back(pKFini);
    mvpLocalMapPoints = mpMap->GetAllMapPoints();
    mpReferenceKF = pKFini;
    mCurrentFrame.mpReferenceKF = pKFini;

    mpMap->SetReferenceMapPoints(mvpLocalMapPoints);

    mpMap->mvpKeyFrameOrigins.push_back(pKFini);

    if (mpViewer)
      mpViewer->setCurrentCameraPose(mCurrentFrame.mTcw);

    mState = OK;
  }
}

void Tracking::MonocularInitialization()
{
  const uint32_t MIN_FEAT = 100;

  // Reset initializer if not enough features
  if (mCurrentFrame.mvKeys.size() <= MIN_FEAT) {
    DEBUG_STREAM("INIT", "Not enough features in frame " << mCurrentFrame.mnId)
    if (mpInitializer) {
      delete mpInitializer;
      mpInitializer = static_cast<Initializer *>(NULL);
      std::fill(mvIniMatches.begin(), mvIniMatches.end(), -1);
    }
  } else if (!mpInitializer) {
    INFO_STREAM("INIT", "Started new initialization at frame " << mCurrentFrame.mnId)
    // Set Reference Frame
    mInitialFrame = Frame(mCurrentFrame);
    mLastFrame = Frame(mCurrentFrame);
    mvbPrevMatched.resize(mCurrentFrame.mvKeysUn.size());
    for (size_t i = 0; i < mCurrentFrame.mvKeysUn.size(); i++)
      mvbPrevMatched[i] = mCurrentFrame.mvKeysUn[i].pt;
    mpInitializer = new Initializer(mCurrentFrame);
    std::fill(mvIniMatches.begin(), mvIniMatches.end(), -1);
  } else {
    // Find correspondences
    ORBmatcher matcher(0.9f, true);
    float decay = MIN_FEAT / 2 * exp(0.14 * mnInitFails);
    uint32_t mnWinSize = std::min(static_cast<uint32_t>(decay), MIN_FEAT);
    size_t nmatches = matcher.SearchForInitialization(mInitialFrame, mCurrentFrame, mvbPrevMatched, mvIniMatches, mnWinSize);

    // Check if there are enough correspondences
    if (nmatches < mnMinMatches) {
      WARN_STREAM("INIT", "Not enough matches: " << nmatches << " / " << mnMinMatches)
      mnInitFails++;
      float decay = MIN_FEAT * exp(-0.14 * mnInitFails);
      mnMinMatches = std::max(static_cast<uint32_t>(decay), MIN_FEAT / 2);
      delete mpInitializer;
      mpInitializer = static_cast<Initializer *>(NULL);
    } else {
      cv::Mat Rcw;                      // Camera Rotation
      cv::Mat tcw;                      // Camera Translation
      std::vector<bool> vbTriangulated; // Triangulated Correspondences (mvIniMatches)
      if (mpInitializer->compute(mCurrentFrame, mvIniMatches, Rcw, tcw, mvIniP3D, vbTriangulated)) {
        for (size_t i = 0, iend = mvIniMatches.size(); i < iend; i++) {
          if (mvIniMatches[i] >= 0 && !vbTriangulated[i]) {
            mvIniMatches[i] = -1;
          }
        }

        // Set Frame Poses and compute initial map
        mInitialFrame.SetPose(cv::Mat::eye(4, 4, CV_32F));
        cv::Mat Tcw = cv::Mat::eye(4, 4, CV_32F);
        Rcw.copyTo(Tcw.rowRange(0, 3).colRange(0, 3));
        tcw.copyTo(Tcw.rowRange(0, 3).col(3));
        mCurrentFrame.SetPose(Tcw);
        CreateInitialMapMonocular();
      }
    }
  }
}

void Tracking::CreateInitialMapMonocular()
{
  // Create KeyFrames
  KeyFrame *pKFini = new KeyFrame(mInitialFrame, mpMap, mpKeyFrameDB);
  KeyFrame *pKFcur = new KeyFrame(mCurrentFrame, mpMap, mpKeyFrameDB);

  // Compute Bag of Words
  pKFini->ComputeBoW();
  pKFcur->ComputeBoW();

  // Insert KFs in the map
  mpMap->AddKeyFrame(pKFini);
  mpMap->AddKeyFrame(pKFcur);

  // Create MapPoints and associate to keyframes
  for (size_t i = 0; i < mvIniMatches.size(); i++) {
    if (mvIniMatches[i] < 0)
      continue;

    cv::Mat worldPos(mvIniP3D[i]);

    MapPoint *pMP = new MapPoint(worldPos, pKFcur, mpMap);

    pKFini->AddMapPoint(pMP, i);
    pKFcur->AddMapPoint(pMP, mvIniMatches[i]);

    pMP->AddObservation(pKFini, i);
    pMP->AddObservation(pKFcur, mvIniMatches[i]);

    pMP->ComputeDistinctiveDescriptors();
    pMP->UpdateNormalAndDepth();

    //Fill Current Frame structure
    mCurrentFrame.mvpMapPoints[mvIniMatches[i]] = pMP;
    mCurrentFrame.mvbOutlier[mvIniMatches[i]] = false;

    //Add to Map
    mpMap->AddMapPoint(pMP);
  }

  // Update Connections
  pKFini->UpdateConnections();
  pKFcur->UpdateConnections();

  // Bundle Adjustment
  Optimizer::GlobalBundleAdjustment(mpMap, 20);

  // Set median depth to 1
  float medianDepth = pKFini->ComputeSceneMedianDepth(2);
  float invMedianDepth = 1.0f / medianDepth;
  if (medianDepth < 0 || pKFcur->TrackedMapPoints(1) < mnMinMatches) { // @fradelg: 100
    DEBUG_STREAM("INIT", "Median Depth: " << medianDepth)
    DEBUG_STREAM("INIT", "MapPoints: " << pKFcur->TrackedMapPoints(1))
    DEBUG_STREAM("INIT", "Wrong initialization. Reseting ...")
    mnInitFails++;
    return Reset();
  }

  // Scale initial baseline
  cv::Mat Tc2w = pKFcur->GetPose();
  Tc2w.col(3).rowRange(0, 3) = Tc2w.col(3).rowRange(0, 3) * invMedianDepth;
  pKFcur->SetPose(Tc2w);

  // Scale points
  std::vector<MapPoint *> vpAllMapPoints = pKFini->GetMapPointMatches();
  for (size_t iMP = 0; iMP < vpAllMapPoints.size(); iMP++) {
    if (vpAllMapPoints[iMP]) {
      MapPoint *pMP = vpAllMapPoints[iMP];
      pMP->SetWorldPos(pMP->GetWorldPos() * invMedianDepth);
    }
  }

  mpLocalMapper->InsertKeyFrame(pKFini);
  mpLocalMapper->InsertKeyFrame(pKFcur);

  mCurrentFrame.SetPose(pKFcur->GetPose());
  mnLastKeyFrameId = mCurrentFrame.mnId;
  mpLastKeyFrame = pKFcur;

  mvpLocalKeyFrames.clear();
  mvpLocalKeyFrames.push_back(pKFcur);
  mvpLocalKeyFrames.push_back(pKFini);
  mvpLocalMapPoints = mpMap->GetAllMapPoints();
  mpReferenceKF = pKFcur;
  mCurrentFrame.mpReferenceKF = pKFcur;

  mLastFrame = Frame(mCurrentFrame);

  mpMap->SetReferenceMapPoints(mvpLocalMapPoints);
  mpMap->mvpKeyFrameOrigins.push_back(pKFini);

  if (mpViewer)
    mpViewer->setCurrentCameraPose(pKFcur->GetPose());

  mState = OK;
  INFO_STREAM("INIT", "Start tracking at " << mInitialFrame.mnId)
  INFO_STREAM("INIT", "New Map with " << mvpLocalMapPoints.size() << " points")
}

void Tracking::CheckReplacedInLastFrame()
{
  for (size_t i = 0; i < mLastFrame.N; i++) {
    MapPoint *pMP = mLastFrame.mvpMapPoints[i];
    if (pMP) {
      MapPoint *pRep = pMP->GetReplaced();
      if (pRep) {
        mLastFrame.mvpMapPoints[i] = pRep;
      }
    }
  }
}

bool Tracking::TrackReferenceKeyFrame()
{
  // Compute Bag of Words vector
  mCurrentFrame.ComputeBoW();

  // We an ORB matching with the reference keyframe
  ORBmatcher matcher(0.7f, true);
  std::vector<MapPoint *> vpMapPointMatches;
  int nmatches = matcher.SearchByBoW(mpReferenceKF, mCurrentFrame, vpMapPointMatches);

  if (nmatches < 15) {
    WARN_STREAM("TrackReferenceKeyFrame", "Track lost with matches: " << nmatches)
    return false;
  }

  mCurrentFrame.mvpMapPoints = vpMapPointMatches;
  mCurrentFrame.SetPose(mLastFrame.mTcw);

  Optimizer::PoseOptimization(&mCurrentFrame);

  // Discard outliers
  int nmatchesMap = 0;
  for (size_t i = 0; i < mCurrentFrame.N; i++) {
    if (mCurrentFrame.mvpMapPoints[i]) {
      if (mCurrentFrame.mvbOutlier[i]) {
        MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
        mCurrentFrame.mvpMapPoints[i] = NULL;
        mCurrentFrame.mvbOutlier[i] = false;
        pMP->mbTrackInView = false;
        pMP->mnLastFrameSeen = mCurrentFrame.mnId;
        nmatches--;
      } else if (mCurrentFrame.mvpMapPoints[i]->Observations() > 0)
        nmatchesMap++;
    }
  }

  if (nmatchesMap < 10)
    WARN_STREAM("TrackReferenceKeyFrame", "Lost track with matches: " << nmatchesMap)

  return nmatchesMap >= 10;
}

// Update pose according to reference keyframe
void Tracking::UpdateLastFrame()
{
  KeyFrame *pRef = mLastFrame.mpReferenceKF;
  cv::Mat Tlr = mlRelativeFramePoses.back();
  mLastFrame.SetPose(Tlr * pRef->GetPose());

  if (mnLastKeyFrameId == mLastFrame.mnId || mSensor == System::MONOCULAR || !mbOnlyTracking)
    return;

  // Create "visual odometry" MapPoints
  // We sort points according to their measured depth by the stereo/RGB-D sensor
  std::vector<std::pair<float, int>> vDepthIdx;
  vDepthIdx.reserve(mLastFrame.N);
  for (size_t i = 0; i < mLastFrame.N; i++) {
    float z = mLastFrame.mvDepth[i];
    if (z > 0) {
      vDepthIdx.push_back(std::make_pair(z, i));
    }
  }

  if (vDepthIdx.empty())
    return;

  std::sort(vDepthIdx.begin(), vDepthIdx.end());

  // We insert all close points (depth<mThDepth)
  // If less than 100 close points, we insert the 100 closest ones.
  int nPoints = 0;
  for (size_t j = 0; j < vDepthIdx.size(); j++) {
    int i = vDepthIdx[j].second;

    bool bCreateNew = false;

    MapPoint *pMP = mLastFrame.mvpMapPoints[i];
    if (!pMP)
      bCreateNew = true;
    else if (pMP->Observations() < 1) {
      bCreateNew = true;
    }

    if (bCreateNew) {
      cv::Mat x3D = mLastFrame.UnprojectStereo(i);
      MapPoint *pNewMP = new MapPoint(x3D, mpMap, &mLastFrame, i);
      mLastFrame.mvpMapPoints[i] = pNewMP;
      mlpTemporalPoints.push_back(pNewMP);
      nPoints++;
    } else {
      nPoints++;
    }

    if (vDepthIdx[j].first > mThDepth && nPoints > 100)
      break;
  }
}

bool Tracking::TrackWithMotionModel()
{
  ORBmatcher matcher(0.9f, true);

  // Update last frame pose according to its reference keyframe
  // If we are in Localization Mode -> Create "visual odometry" points
  UpdateLastFrame();

  // Estimate pose according to motion model
  mCurrentFrame.SetPose(mVelocity * mLastFrame.mTcw);

  // Project map points from previous frame; uses a wider window search if few matches are found
  int nmatches;
  for (int it = 1; it < 3; ++it) {
    int th = mSensor != System::STEREO ? 15 : 7;
    std::fill(mCurrentFrame.mvpMapPoints.begin(), mCurrentFrame.mvpMapPoints.end(), static_cast<MapPoint *>(NULL));
    nmatches = matcher.SearchByProjection(mCurrentFrame, mLastFrame, th * it, mSensor == System::MONOCULAR);
    if (nmatches > 20)
      break;
  }

  if (nmatches < 20) {
    WARN_STREAM("TrackWithMotionModel", "Not enough ORB matches " << nmatches << " / 20")
    return false;
  }

  // Optimize frame pose with all matches
  Optimizer::PoseOptimization(&mCurrentFrame);

  // Discard map outliers
  int nmatchesMap = 0;
  for (size_t i = 0; i < mCurrentFrame.N; i++) {
    if (mCurrentFrame.mvpMapPoints[i]) {
      if (mCurrentFrame.mvbOutlier[i]) {
        MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
        pMP->mbTrackInView = false;
        pMP->mnLastFrameSeen = mCurrentFrame.mnId;
        mCurrentFrame.mvpMapPoints[i] = NULL;
        mCurrentFrame.mvbOutlier[i] = false;
      } else if (mCurrentFrame.mvpMapPoints[i]->Observations() > 0)
        nmatchesMap++;
    }
  }

  if (mbOnlyTracking) {
    mbVO = nmatchesMap < 10;
    return nmatches > 20;
  }

  if (nmatchesMap < 10) {
    WARN_STREAM("TrackWithMotionModel", "Not enough MapPoints: " << nmatchesMap)
  }

  return nmatchesMap >= 10;
}

// We have an estimation of the camera pose and some map points tracked in the frame.
// We augmented the local map and try to find matches to points in the local map.
bool Tracking::TrackLocalMap()
{
  UpdateLocalMap();
  SearchLocalPoints();

  // Optimize Pose
  Optimizer::PoseOptimization(&mCurrentFrame);
  mnMatchesInliers = 0;

  // Update MapPoints Statistics
  for (size_t i = 0; i < mCurrentFrame.N; i++) {
    if (mCurrentFrame.mvpMapPoints[i]) {
      if (!mCurrentFrame.mvbOutlier[i]) {
        mCurrentFrame.mvpMapPoints[i]->IncreaseFound();
        if (!mbOnlyTracking) {
          if (mCurrentFrame.mvpMapPoints[i]->Observations() > 0)
            mnMatchesInliers++;
        } else
          mnMatchesInliers++;
      } else if (mSensor == System::STEREO)
        mCurrentFrame.mvpMapPoints[i] = static_cast<MapPoint *>(NULL);
    }
  }

  // Decide if the tracking was succesful
  // More restrictive if there was a recent relocalization
  bool bReloc = mCurrentFrame.mnId < mnLastRelocFrameId + mMaxFrames;
  bool bRes = bReloc && (mnMatchesInliers < 50);
  if (mnMatchesInliers < 20)
    WARN_STREAM("TrackLocalMap", "Tracking MapPoints: " << mnMatchesInliers)
  return (mnMatchesInliers > 20) && !bRes; // @fradelg: 30
}


bool Tracking::NeedNewKeyFrame()
{
  if (mbOnlyTracking)
    return false;

  // If Local Mapping is freezed by a Loop Closure do not insert keyframes
  if (mpLocalMapper->isStopped() || mpLocalMapper->stopRequested())
    return false;

  const size_t nKFs = mpMap->KeyFramesInMap();

  // Do not insert keyframes if not enough frames have passed from last relocalisation
  if (mCurrentFrame.mnId < mnLastRelocFrameId + mMaxFrames && nKFs > mMaxFrames)
    return false;

  // Local Mapping accept keyframes?
  bool bLocalMappingIdle = mpLocalMapper->AcceptKeyFrames();

  // Check how many "close" points are being tracked and how many could be potentially created.
  int nNonTrackedClose = 0;
  int nTrackedClose = 0;
  if (mSensor != System::MONOCULAR) {
    for (size_t i = 0; i < mCurrentFrame.N; i++) {
      if (mCurrentFrame.mvDepth[i] > 0 && mCurrentFrame.mvDepth[i] < mThDepth) {
        if (mCurrentFrame.mvpMapPoints[i] && !mCurrentFrame.mvbOutlier[i])
          nTrackedClose++;
        else
          nNonTrackedClose++;
      }
    }
  }

  bool bNeedToInsertClose = (nTrackedClose < 100) && (nNonTrackedClose > 70);

  // Thresholds
  float thRefRatio = nKFs < 2 ? 0.4f : 0.75f;
  if (mSensor == System::MONOCULAR)
    thRefRatio = 0.9f;

  // Tracked MapPoints in the reference keyframe
  int nMinObs = nKFs < 3 ? 2 : 3;
  int nRefMatches = mpReferenceKF->TrackedMapPoints(nMinObs);

  // Condition 1a: More than "MaxFrames" have passed from last keyframe insertion
  const bool c1a = mCurrentFrame.mnId >= mnLastKeyFrameId + mMaxFrames;
  // Condition 1b: More than "MinFrames" have passed and Local Mapping is idle
  const bool c1b = (mCurrentFrame.mnId >= mnLastKeyFrameId + mMinFrames && bLocalMappingIdle);
  // Condition 1c: tracking is weak
  const bool c1c = mSensor != System::MONOCULAR && (mnMatchesInliers < nRefMatches * 0.25 || bNeedToInsertClose);
  // Condition 2: Few tracked points compared to reference keyframe. Lots of visual odometry compared to map matches.
  const bool c2 = ((mnMatchesInliers < nRefMatches * thRefRatio || bNeedToInsertClose) && mnMatchesInliers > 15);

  if ((c1a || c1b || c1c) && c2) {
    // If the mapping accepts keyframes, insert keyframe.
    // Otherwise send a signal to interrupt BA
    if (bLocalMappingIdle) {
      return true;
    } else {
      mpLocalMapper->InterruptBA();
      return (mSensor != System::MONOCULAR) && (mpLocalMapper->KeyframesInQueue() < 3);
    }
  } else
    return false;
}

void Tracking::CreateNewKeyFrame()
{
  // Avoid local mapping stopping while the KeyFrame is being created
  if (!mpLocalMapper->SetNotStop(true))
    return;

  KeyFrame *pKF = new KeyFrame(mCurrentFrame, mpMap, mpKeyFrameDB);
  mpReferenceKF = pKF;
  mCurrentFrame.mpReferenceKF = pKF;

  if (mSensor != System::MONOCULAR) {
    mCurrentFrame.UpdatePoseMatrices();

    // We sort points by the measured depth by the stereo/RGBD sensor.
    // We create all those MapPoints whose depth < mThDepth.
    // If there are less than 100 close points we create the 100 closest.
    std::vector<std::pair<float, size_t>> vDepthIdx;
    vDepthIdx.reserve(mCurrentFrame.N);
    for (size_t i = 0; i < mCurrentFrame.N; i++) {
      float z = mCurrentFrame.mvDepth[i];
      if (z > 0) {
        vDepthIdx.push_back(std::make_pair(z, i));
      }
    }

    if (!vDepthIdx.empty()) {
      std::sort(vDepthIdx.begin(), vDepthIdx.end());

      int nPoints = 0;
      for (size_t j = 0; j < vDepthIdx.size(); j++) {
        size_t i = vDepthIdx[j].second;

        bool bCreateNew = false;

        MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
        if (!pMP)
          bCreateNew = true;
        else if (pMP->Observations() < 1) {
          bCreateNew = true;
          mCurrentFrame.mvpMapPoints[i] = static_cast<MapPoint *>(NULL);
        }

        if (bCreateNew) {
          cv::Mat x3D = mCurrentFrame.UnprojectStereo(i);
          MapPoint *pNewMP = new MapPoint(x3D, pKF, mpMap);
          pNewMP->AddObservation(pKF, i);
          pKF->AddMapPoint(pNewMP, i);
          pNewMP->ComputeDistinctiveDescriptors();
          pNewMP->UpdateNormalAndDepth();
          mpMap->AddMapPoint(pNewMP);
          mCurrentFrame.mvpMapPoints[i] = pNewMP;
          nPoints++;
        } else {
          nPoints++;
        }

        if (vDepthIdx[j].first > mThDepth && nPoints > 100)
          break;
      }
    }
  }

  mpLocalMapper->InsertKeyFrame(pKF);
  mpLocalMapper->SetNotStop(false);

  mnLastKeyFrameId = mCurrentFrame.mnId;
  mpLastKeyFrame = pKF;
}

void Tracking::SearchLocalPoints()
{
  // Do not search map points already matched in the current frame
  for (size_t i = 0; i < mCurrentFrame.mvpMapPoints.size(); ++i) {
    MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
    if (pMP) {
      if (pMP->isBad()) {
        mCurrentFrame.mvpMapPoints[i] = NULL;
      } else {
        pMP->IncreaseVisible();
        pMP->mnLastFrameSeen = mCurrentFrame.mnId;
        pMP->mbTrackInView = false;
      }
    }
  }

  bool bToMatch = false;

  // Check visibility in current frame for the leftovers
  for (MapPoint *pMP : mvpLocalMapPoints) {
    if (pMP->isBad() || pMP->mnLastFrameSeen == mCurrentFrame.mnId)
      continue;

    // Exclude from search by default
    pMP->mbTrackInView = false;

    // Check visibility (this also fills MapPoint variables for matching)
    if (mCurrentFrame.isInFrustum(pMP, 0.5)) {
      pMP->IncreaseVisible();
      bToMatch = true;
    }
  }

  if (bToMatch) {
    ORBmatcher matcher(0.8f);
    int th = mSensor == System::RGBD ? 3 : 1;
    // If the camera has been relocalised recently, perform a coarser search
    if (mCurrentFrame.mnId < mnLastRelocFrameId + 3) // @fradelg: 2
      th = 5;
    matcher.SearchByProjection(mCurrentFrame, mvpLocalMapPoints, th);
  }
}

void Tracking::UpdateLocalMap()
{
  // This is for visualization
  mpMap->SetReferenceMapPoints(mvpLocalMapPoints);

  // Update
  UpdateLocalKeyFrames();
  UpdateLocalPoints();
}

void Tracking::UpdateLocalKeyFrames()
{
  // Each map point vote for the keyframes in which it has been observed
  std::map<KeyFrame *, size_t> keyframeCounter;
  for (size_t i = 0; i < mCurrentFrame.N; i++) {
    MapPoint *pMP = mCurrentFrame.mvpMapPoints[i];
    if (pMP && !pMP->isBad()) {
      for (auto &obs : pMP->GetObservations())
        keyframeCounter[obs.first]++;
    } else {
      mCurrentFrame.mvpMapPoints[i] = NULL;
    }
  }

  if (keyframeCounter.empty())
    return;

  std::pair<KeyFrame *, size_t> best(NULL, 0);
  mvpLocalKeyFrames.clear();
  mvpLocalKeyFrames.reserve(3 * keyframeCounter.size());

  // All keyframes that observe a map point are included in the local map. Also check which keyframe shares most points
  for (auto &obs : keyframeCounter) {
    KeyFrame *pKF = obs.first;

    if (pKF->isBad())
      continue;

    if (obs.second > best.second) {
      best = obs;
    }

    mvpLocalKeyFrames.push_back(obs.first);
    pKF->mnTrackReferenceForFrame = mCurrentFrame.mnId;
  }

  // Include also some not-already-included keyframes that are neighbors to already-included keyframes
  for (KeyFrame *pKF : mvpLocalKeyFrames) {
    // Limit the number of keyframes
    if (mvpLocalKeyFrames.size() > 100)
      break;

    for (KeyFrame *pNeighKF : pKF->GetBestCovisibilityKeyFrames(10)) {
      if (!pNeighKF->isBad()) {
        if (pNeighKF->mnTrackReferenceForFrame != mCurrentFrame.mnId) {
          mvpLocalKeyFrames.push_back(pNeighKF);
          pNeighKF->mnTrackReferenceForFrame = mCurrentFrame.mnId;
          break;
        }
      }
    }

    for (KeyFrame *pChildKF : pKF->GetChilds()) {
      if (!pChildKF->isBad()) {
        if (pChildKF->mnTrackReferenceForFrame != mCurrentFrame.mnId) {
          mvpLocalKeyFrames.push_back(pChildKF);
          pChildKF->mnTrackReferenceForFrame = mCurrentFrame.mnId;
          break;
        }
      }
    }

    KeyFrame *pParent = pKF->GetParent();
    if (pParent) {
      if (pParent->mnTrackReferenceForFrame != mCurrentFrame.mnId) {
        mvpLocalKeyFrames.push_back(pParent);
        pParent->mnTrackReferenceForFrame = mCurrentFrame.mnId;
        break;
      }
    }
  }

  // Save the reference KeyFrame
  if (best.first) {
    mpReferenceKF = best.first;
    mCurrentFrame.mpReferenceKF = mpReferenceKF;
  }
}

void Tracking::UpdateLocalPoints()
{
  mvpLocalMapPoints.clear();

  for (KeyFrame *pKF : mvpLocalKeyFrames) {
    for (MapPoint *pMP : pKF->GetMapPointMatches()) {
      if (pMP && !pMP->isBad() && pMP->mnTrackReferenceForFrame != mCurrentFrame.mnId) {
        mvpLocalMapPoints.push_back(pMP);
        pMP->mnTrackReferenceForFrame = mCurrentFrame.mnId;
      }
    }
  }
}

bool Tracking::Relocalization()
{
  // Compute Bag of Words Vector
  mCurrentFrame.ComputeBoW();

  // Track Lost: Query KeyFrame Database for keyframe candidates for relocalisation
  std::vector<KeyFrame *> vpCandidateKFs = mpKeyFrameDB->DetectRelocalizationCandidates(&mCurrentFrame);

  if (vpCandidateKFs.empty())
    return false;

  const size_t nKFs = vpCandidateKFs.size();

  // We perform first an ORB matching with each candidate
  // If enough matches are found we setup a PnP solver
  ORBmatcher matcher(0.75, true);

  std::vector<PnPsolver *> vpPnPsolvers;
  vpPnPsolvers.resize(nKFs);

  std::vector<std::vector<MapPoint *>> vvpMapPointMatches;
  vvpMapPointMatches.resize(nKFs);

  std::vector<bool> vbDiscarded;
  vbDiscarded.resize(nKFs);

  int nCandidates = 0;

  for (size_t i = 0; i < nKFs; i++) {
    KeyFrame *pKF = vpCandidateKFs[i];
    if (pKF->isBad())
      vbDiscarded[i] = true;
    else {
      int nmatches = matcher.SearchByBoW(pKF, mCurrentFrame, vvpMapPointMatches[i]);
      if (nmatches < 15) {
        vbDiscarded[i] = true;
        continue;
      } else {
        PnPsolver *pSolver = new PnPsolver(mCurrentFrame, vvpMapPointMatches[i]);
        pSolver->SetRansacParameters(0.99, 10, 300, 4, 0.5, 5.991f);
        vpPnPsolvers[i] = pSolver;
        nCandidates++;
      }
    }
  }

  // Alternatively perform some iterations of P4P RANSAC
  // Until we found a camera pose supported by enough inliers
  ORBmatcher matcher2(0.9f, true);
  bool bMatch = false;
  while (!bMatch && nCandidates > 0) {
    for (size_t i = 0; i < nKFs && !bMatch; i++) {
      if (vbDiscarded[i])
        continue;

      // Perform five more RANSAC iterations
      std::vector<bool> vbInliers;
      uint32_t nInliers;
      bool bNoMore;
      PnPsolver *pSolver = vpPnPsolvers[i];
      cv::Mat Tcw = pSolver->iterate(5, bNoMore, vbInliers, nInliers);

      // If Ransac reachs max. iterations discard keyframe
      if (bNoMore) {
        vbDiscarded[i] = true;
        nCandidates--;
      }

      // If a Camera Pose is computed, optimize
      if (!Tcw.empty()) {
        Tcw.copyTo(mCurrentFrame.mTcw);

        std::set<MapPoint *> sFound;
        for (size_t j = 0; j < vbInliers.size(); j++) {
          if (vbInliers[j]) {
            mCurrentFrame.mvpMapPoints[j] = vvpMapPointMatches[i][j];
            sFound.insert(vvpMapPointMatches[i][j]);
          } else
            mCurrentFrame.mvpMapPoints[j] = NULL;
        }

        int nGood = Optimizer::PoseOptimization(&mCurrentFrame);

        if (nGood < 10)
          continue;

        for (size_t io = 0; io < mCurrentFrame.N; io++)
          if (mCurrentFrame.mvbOutlier[io])
            mCurrentFrame.mvpMapPoints[io] = NULL;

        // If few inliers, search by projection in a coarser window and optimize again
        if (nGood < 50) {
          int nadditional = matcher2.SearchByProjection(mCurrentFrame, vpCandidateKFs[i], sFound, 10, 100);

          if (nadditional + nGood >= 50) {
            nGood = Optimizer::PoseOptimization(&mCurrentFrame);

            // If many inliers but still not enough, search by projection again in a narrower window
            // the camera has been already optimized with many points
            if (nGood > 30 && nGood < 50) {
              sFound.clear();
              for (size_t ip = 0; ip < mCurrentFrame.N; ip++)
                if (mCurrentFrame.mvpMapPoints[ip])
                  sFound.insert(mCurrentFrame.mvpMapPoints[ip]);
              nadditional = matcher2.SearchByProjection(mCurrentFrame, vpCandidateKFs[i], sFound, 3, 64);

              // Final optimization
              if (nGood + nadditional >= 50) {
                nGood = Optimizer::PoseOptimization(&mCurrentFrame);

                for (size_t io = 0; io < mCurrentFrame.N; io++)
                  if (mCurrentFrame.mvbOutlier[io])
                    mCurrentFrame.mvpMapPoints[io] = NULL;
              }
            }
          }
        }

        // Check if the pose is supported by enough inliers
        bMatch = nGood >= 50;
      }
    }
  }

  if (bMatch)
    mnLastRelocFrameId = mCurrentFrame.mnId;
  return bMatch;
}

void Tracking::Reset()
{
  INFO_STREAM("TRACKING", "System Reseting ...")
  mpLocalMapper->RequestReset();
  mpLoopClosing->RequestReset();
  mpKeyFrameDB->clear();

  // Clear Map (this erase MapPoints and KeyFrames)
  mpMap->clear();

  KeyFrame::nNextId = 0;
  Frame::nNextId = 0;
  mState = NO_IMAGES_YET;

  if (mpInitializer) {
    delete mpInitializer;
    mpInitializer = static_cast<Initializer *>(NULL);
  }

  mlRelativeFramePoses.clear();
  mlpReferences.clear();
  mlFrameTimes.clear();
  mlbLost.clear();
}

void Tracking::LoadCalibration()
{
  float fx = Settings::getInstance().get("Camera.fx");
  float fy = Settings::getInstance().get("Camera.fy");
  float cx = Settings::getInstance().get("Camera.cx");
  float cy = Settings::getInstance().get("Camera.cy");

  cv::Mat K = cv::Mat::eye(3, 3, CV_32F);
  K.at<float>(0, 0) = fx;
  K.at<float>(1, 1) = fy;
  K.at<float>(0, 2) = cx;
  K.at<float>(1, 2) = cy;
  K.copyTo(mK);

  cv::Mat DistCoef(4, 1, CV_32F);
  DistCoef.at<float>(0) = Settings::getInstance().get("Camera.k1");
  DistCoef.at<float>(1) = Settings::getInstance().get("Camera.k2");
  DistCoef.at<float>(2) = Settings::getInstance().get("Camera.p1");
  DistCoef.at<float>(3) = Settings::getInstance().get("Camera.p2");
  const float k3 = Settings::getInstance().get("Camera.k3");
  if (k3 != 0.0f) {
    DistCoef.resize(5);
    DistCoef.at<float>(4) = k3;
  }
  DistCoef.copyTo(mDistCoef);

  mbf = Settings::getInstance().get("Camera.bf");

  Frame::mbInitialComputations = true;
}

void Tracking::InformOnlyTracking(const bool &flag)
{
  mbOnlyTracking = flag;
}


} // namespace slam
