#include <Settings.h>

namespace slam
{

Settings &Settings::getInstance()
{
  static Settings inst;
  return inst;
}

Settings::Settings()
{
}

Settings::~Settings()
{
  mSettings.release();
}

void Settings::loadFrom(const std::string &fileName)
{
  mSettings.open(fileName, cv::FileStorage::READ);
  if (!mSettings.isOpened()) {
    throw new std::runtime_error("Failed to open settings at " + fileName);
  }
}

cv::FileNode Settings::get(const std::string &nodeName)
{
  return mSettings[nodeName];
}

cv::FileNode Settings::operator[](const std::string &nodeName)
{
  return get(nodeName);
}
}
