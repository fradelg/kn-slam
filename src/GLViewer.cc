/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdint>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

#include <nanogui/glutil.h>
#include <nanogui/imageview.h>
#include <nanogui/label.h>
#include <nanogui/layout.h>
#include <nanogui/opengl.h>
#include <nanogui/popupbutton.h>
#include <nanogui/screen.h>
#include <nanogui/window.h>

#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>

#include <GLViewer.h>
#include <Map.h>
#include <Plane.h>
#include <Settings.h>
#include <System.h>
#include <Tracking.h>
#include <global.h>

#if defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
#if defined(_WIN32)
#pragma warning(push)
#pragma warning(disable : 4457 4456 4005 4312)
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace slam
{

GLTexture::GLTexture(const std::string &textureName)
    : mTextureName(textureName)
    , mTextureId(0)
{
}

GLTexture::GLTexture(const std::string &textureName, GLuint textureId)
    : mTextureName(textureName)
    , mTextureId(textureId)
{
}

GLTexture::GLTexture(GLTexture &&other) noexcept
    : mTextureName(std::move(other.mTextureName))
    , mTextureId(other.mTextureId)
{
  other.mTextureId = 0;
}

GLTexture &GLTexture::operator=(GLTexture &&other) noexcept
{
  mTextureName = std::move(other.mTextureName);
  std::swap(mTextureId, other.mTextureId);
  return *this;
}

GLTexture::~GLTexture() noexcept
{
  if (mTextureId)
    glDeleteTextures(1, &mTextureId);
}

GLTexture::HandleType GLTexture::load(const std::string &fileName)
{
  if (mTextureId) {
    glDeleteTextures(1, &mTextureId);
    mTextureId = 0;
  }

  int force_channels = 0;
  int w, h, n;
  GLTexture::HandleType textureData(stbi_load(fileName.c_str(), &w, &h, &n, force_channels), stbi_image_free);
  if (!textureData)
    throw std::invalid_argument("Could not load texture data from file " + fileName);

  glGenTextures(1, &mTextureId);
  glBindTexture(GL_TEXTURE_2D, mTextureId);
  GLint internalFormat;
  GLint format;
  switch (n) {
  case 1:
    internalFormat = GL_R8;
    format = GL_RED;
    break;
  case 2:
    internalFormat = GL_RG8;
    format = GL_RG;
    break;
  case 3:
    internalFormat = GL_RGB8;
    format = GL_RGB;
    break;
  case 4:
    internalFormat = GL_RGBA8;
    format = GL_RGBA;
    break;
  default:
    internalFormat = 0;
    format = 0;
    break;
  }
  glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, format, GL_UNSIGNED_BYTE, textureData.get());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  return textureData;
}

void GLTexture::load(cv::Mat &image)
{
  if (image.empty())
    std::runtime_error("load texture error: image is empty");

  if (mTextureId) {
    glDeleteTextures(1, &mTextureId);
    mTextureId = 0;
  }

  //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glGenTextures(1, &mTextureId);
  glBindTexture(GL_TEXTURE_2D, mTextureId);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glTexImage2D(GL_TEXTURE_2D, // Type of texture
      0,                      // Pyramid level (for mip-mapping) - 0 is the top level
      GL_RGB8,                // Internal colour format to convert to
      image.cols,             // Image width  i.e. 640 for Kinect in standard mode
      image.rows,             // Image height i.e. 480 for Kinect in standard mode
      0,                      // Border width in pixels (can either be 1 or 0)
      GL_RGB,                 // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
      GL_UNSIGNED_BYTE,       // Image data type
      image.ptr());           // The actual image data itself
}

void GLTexture::update(cv::Mat &image)
{
  glBindTexture(GL_TEXTURE_2D, mTextureId);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.cols, image.rows, GL_RGB, GL_UNSIGNED_BYTE, image.ptr());
}


Viewer::Viewer(const std::string &title, System *system, Map *map)
    : nanogui::Screen(Eigen::Vector2i(1280, 960), title)
    , mpSystem(system)
    , mpMap(map)
    , mTex("tex")
    , mCameraPose(cv::Mat::eye(4, 4, CV_32F))
    , mPlane(NULL)
    , mActive(false)
    , mZoom(1.0f)
    , mArcBall(4.0f)
    , mbShowMap(true)
    , mbShowKFs(true)
    , mbShowGraph(true)
    , mbFollowCamera(true)
    , mbShowFrame(true)
    , mbExitFinish(true)
    , mbTranslate(false)
    , mTexSize(640.0f, 480.0f)
    , mCurrentFrame(480, 640, CV_8U, cv::Scalar(0, 0, 0))
    , mState(Tracking::SYSTEM_NOT_READY)
{
  loadSettings();

  mArcBall.setSize(mSize);
  mTranslation << 0.0f, 0.0f;

  auto window = new nanogui::Window(this, "Actions");
  window->setPosition(Eigen::Vector2i(0, 0));
  window->setLayout(new nanogui::GroupLayout(0, 0, 0, 0));

  nanogui::PopupButton *openBtn = new nanogui::PopupButton(window, "Menu");
  openBtn->setIcon(ENTYPO_ICON_MEGAPHONE);
  nanogui::Popup *popup = openBtn->popup();
  popup->setLayout(new nanogui::GroupLayout());

  nanogui::Widget *statePanel = new nanogui::Widget(popup);
  statePanel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0, 5));
  nanogui::Button *planeBtn = new nanogui::Button(statePanel, "Plane", ENTYPO_ICON_AIR);
  planeBtn->setCallback([&] { this->detectPlane(); });
  nanogui::Button *resetBtn = new nanogui::Button(statePanel, "Reset", ENTYPO_ICON_CROSS);
  resetBtn->setCallback([&] { mpSystem->Reset(); });
  nanogui::Button *locateBtn = new nanogui::Button(statePanel, "Localization", ENTYPO_ICON_LOCATION);
  locateBtn->setCallback([&] { mpSystem->ActivateLocalizationMode(); });
  nanogui::Button *slamBtn = new nanogui::Button(statePanel, "SLAM mode", ENTYPO_ICON_BROWSER);
  slamBtn->setCallback([&] { mpSystem->DeactivateLocalizationMode(); });
  nanogui::Button *exitBtn = new nanogui::Button(statePanel, "Hide", ENTYPO_ICON_LOG_OUT);
  exitBtn->setFlags(nanogui::Button::ToggleButton);

  nanogui::PopupButton *showBtn = new nanogui::PopupButton(window, "Show");
  showBtn->setIcon(ENTYPO_ICON_EYE);
  popup = showBtn->popup();
  popup->setLayout(new nanogui::GroupLayout());

  nanogui::Widget *showPanel = new nanogui::Widget(popup);
  showPanel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0, 5));
  nanogui::Button *showPointsBtn = new nanogui::Button(showPanel, "Points", ENTYPO_ICON_CLOUD);
  showPointsBtn->setPushed(mbShowMap);
  showPointsBtn->setFlags(nanogui::Button::ToggleButton);
  showPointsBtn->setChangeCallback([&](bool state) { mbShowMap = state; });
  nanogui::Button *showKfBtn = new nanogui::Button(showPanel, "KeyFrames", ENTYPO_ICON_CAMERA);
  showKfBtn->setPushed(mbShowKFs);
  showKfBtn->setFlags(nanogui::Button::ToggleButton);
  showKfBtn->setChangeCallback([&](bool state) { mbShowKFs = state; });
  nanogui::Button *showGraphBtn = new nanogui::Button(showPanel, "Graph", ENTYPO_ICON_LINE_GRAPH);
  showGraphBtn->setPushed(mbShowGraph);
  showGraphBtn->setFlags(nanogui::Button::ToggleButton);
  showGraphBtn->setChangeCallback([&](bool state) { mbShowGraph = state; });
  nanogui::Button *showFrameBtn = new nanogui::Button(showPanel, "Frame", ENTYPO_ICON_IMAGE);
  showFrameBtn->setPushed(mbShowFrame);
  showFrameBtn->setFlags(nanogui::Button::ToggleButton);
  showFrameBtn->setChangeCallback([&](bool state) { mbShowFrame = state; });
  nanogui::Button *followCameraBtn = new nanogui::Button(showPanel, "Follow", ENTYPO_ICON_TRAFFIC_CONE);
  followCameraBtn->setPushed(mbFollowCamera);
  followCameraBtn->setFlags(nanogui::Button::ToggleButton);
  followCameraBtn->setChangeCallback([&](bool state) { mbFollowCamera = state; });
  nanogui::Button *exitFinishBtn = new nanogui::Button(showPanel, "Exit at finish", ENTYPO_ICON_AIRCRAFT);
  exitFinishBtn->setPushed(mbFollowCamera);
  exitFinishBtn->setFlags(nanogui::Button::ToggleButton);
  exitFinishBtn->setChangeCallback([&](bool state) { mbExitFinish = state; });

  // Create a ImageView widget for the current frame
  mFrameWindow = new nanogui::Window(this, "Current Frame");
  mFrameWindow->setPosition(Eigen::Vector2i(0, 0));
  mFrameWindow->setLayout(new nanogui::GroupLayout(0, 0, 0, 0));
  drawFrame();
  mTex.load(mTexture);
  auto imageView = new nanogui::ImageView(mFrameWindow, mTex.texture());
  imageView->setPosition(Eigen::Vector2i(0, 0));

  performLayout();

  mViewMat = nanogui::lookAt(
      mViewpoint,
      Eigen::Vector3f(0.0, 0.0, 0.0),
      Eigen::Vector3f(0.0, -1.0, 0.0));

  const float viewAngle = 60.0;
  const float dnear = 0.01f;
  const float dfar = 1000.0f;
  float fH = std::tan(viewAngle / 360.0f * static_cast<float>(M_PI)) * dnear;
  float fW = fH * mSize.x() / mSize.y();
  mProjMat = nanogui::frustum(-fW, fW, -fH, fH, dnear, dfar);

  glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

  createMapShader();
  createPlaneShader();
  createKeyFrameShader();
  createGraphShader();
}

Viewer::~Viewer()
{
  mMapShader.free();
  mKFShader.free();
  mGraphShader.free();
  mPlaneShader.free();
  if (mPlane)
    delete mPlane;
}

bool Viewer::keyboardEvent(int key, int scancode, int action, int modifiers)
{
  if (Screen::keyboardEvent(key, scancode, action, modifiers))
    return true;

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    setVisible(false);
    return true;
  }

  return false;
}

bool Viewer::mouseButtonEvent(const Eigen::Vector2i &p, int button, bool down, int modifiers)
{
  mActive = down;
  if (!Screen::mouseButtonEvent(p, button, down, modifiers)) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
      mArcBall.button(p, down);
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
      mbTranslate = down;
      mMousePosition = p;
    }
  }
  return true;
}

bool Viewer::mouseMotionEvent(const Eigen::Vector2i &p, const Eigen::Vector2i &rel, int button, int modifiers)
{
  if (!mActive)
    return false;

  if (!Screen::mouseMotionEvent(p, rel, button, modifiers)) {
    mArcBall.motion(p);
    if (mbTranslate) {
      Eigen::Vector2f diff = (p - mMousePosition).cast<float>();
      mTranslation(0) += diff(0) / mSize(0) * 2.0f;
      mTranslation(1) -= diff(1) / mSize(1) * 2.0f;
      mMousePosition = p;
    }
  }
  return true;
}

bool Viewer::scrollEvent(const Eigen::Vector2i & /*p*/, const Eigen::Vector2f &rel)
{
  mZoom *= (1.0f + 0.1f * rel.y());
  return false;
}

void Viewer::loadSettings()
{
  Settings &fSettings = Settings::getInstance();
  mKeyFrameSize = fSettings["Viewer.KeyFrameSize"];
  mKeyFrameLineWidth = fSettings["Viewer.KeyFrameLineWidth"];
  mGraphLineWidth = fSettings["Viewer.GraphLineWidth"];
  mPointSize = fSettings["Viewer.PointSize"];
  mCameraSize = fSettings["Viewer.CameraSize"];
  mCameraSize += 1.0;
  mCameraLineWidth = fSettings["Viewer.CameraLineWidth"];
  mViewpoint << fSettings["Viewer.ViewpointX"], fSettings["Viewer.ViewpointY"], fSettings["Viewer.ViewpointZ"];
}

void Viewer::draw(NVGcontext *ctx)
{
  Screen::draw(ctx);
}

bool Viewer::resizeEvent(const Eigen::Vector2i &size)
{
  mArcBall.setSize(size);
  return true;
}

void Viewer::setCurrentCameraPose(const cv::Mat &Tcw)
{
  std::unique_lock<std::mutex> lock(mMutexCamera);
  mCameraPose = Tcw;
}

Eigen::Matrix4f Viewer::getViewProjectionMatrix()
{
  cv::Mat pose = cv::Mat::eye(4, 4, CV_32F);
  if (mbFollowCamera) {
    std::unique_lock<std::mutex> lock(mMutexCamera);
    pose = mCameraPose.t(); // OpenGL saves matrix in column-wise
  }

  Eigen::Matrix4f epose(pose.ptr<float>());
  Eigen::Matrix4f rotation = mArcBall.matrix();
  Eigen::Matrix4f scale = Eigen::Matrix4f::Identity();
  Eigen::Matrix4f translation = Eigen::Matrix4f::Identity();
  scale(0, 0) = mZoom;
  scale(1, 1) = mZoom;
  scale(2, 2) = mZoom;
  translation(0, 3) = mTranslation(0);
  translation(1, 3) = mTranslation(1);
  return translation * mProjMat * mViewMat * scale * epose * rotation;
}

void Viewer::drawContents()
{
  drawCamera();
  drawPlane();
  if (mbShowMap)
    drawMapPoints();
  if (mbShowKFs)
    drawKeyFrames();
  if (mbShowGraph)
    drawGraph();

  // Update texture of ImageView (save in a member for texturing data)
  mFrameWindow->setVisible(mbShowFrame);
  if (mbShowFrame) {
    drawFrame();
    mTex.update(mTexture);
  }
}

void Viewer::drawMapPoints()
{
  const std::vector<MapPoint *> &vpMPs = mpMap->GetAllMapPoints();

  if (vpMPs.empty())
    return;

  Eigen::MatrixXf positions(3, vpMPs.size());
  Eigen::MatrixXf colors(3, vpMPs.size());
  for (size_t i = 0, iend = vpMPs.size(); i < iend; i++) {
    if (!vpMPs[i]->isBad()) {
      cv::Mat pos = vpMPs[i]->GetWorldPos();
      cv::Vec3f color = vpMPs[i]->GetRGB();
      colors.col(i) << color[2], color[1], color[0];
      positions.col(i) << pos.at<float>(0), pos.at<float>(1), pos.at<float>(2);
    }
  }

  Eigen::Matrix4f mvp = getViewProjectionMatrix();
  mMapShader.bind();
  mMapShader.uploadAttrib("position", positions);
  mMapShader.uploadAttrib("color", colors);
  mMapShader.setUniform("mvp", mvp);
  mMapShader.drawArray(GL_POINTS, 0, vpMPs.size());
}

void Viewer::drawCamera()
{
  cv::Mat Tcw;
  {
    std::unique_lock<std::mutex> lock(mMutexCamera);
    Tcw = mCameraPose;
  }

  // Compute the inverse camera pose
  cv::Mat Rwc = Tcw.rowRange(0, 3).colRange(0, 3).t();
  cv::Mat tcw = Tcw.rowRange(0, 3).col(3);
  cv::Mat Ow = -Rwc * tcw;

  cv::Mat Twc = cv::Mat::eye(4, 4, CV_32F);
  Rwc.copyTo(Twc.rowRange(0, 3).colRange(0, 3));
  Ow.copyTo(Twc.rowRange(0, 3).col(3));
  Twc = Twc.t();

  // Compute the MVP matrix
  Eigen::Matrix4f invpose(Twc.ptr<float>());
  Eigen::Matrix4f mvp = getViewProjectionMatrix() * invpose;
  mvp(0, 0) *= mCameraSize;
  mvp(1, 1) *= mCameraSize;
  mvp(2, 2) *= mCameraSize;

  mKFShader.bind();
  mKFShader.setUniform("color", nanogui::Color(0.0f, 0.0f, 1.0f, 1.0f));
  mKFShader.setUniform("mvp", mvp);
  mKFShader.drawIndexed(GL_LINES, 0, 8);
}

void Viewer::drawKeyFrames()
{
  mKFShader.bind();
  mKFShader.setUniform("color", nanogui::Color(1.0f, 0.0f, 0.0f, 1.0f));
  for (KeyFrame *pKF : mpMap->GetAllKeyFrames()) {
    cv::Mat Twc = pKF->GetPoseInverse().t(); // OpenGL uses column-wise matrices
    Eigen::Matrix4f model(Twc.ptr<float>());
    Eigen::Matrix4f mvp = getViewProjectionMatrix() * model;
    mKFShader.setUniform("mvp", mvp);
    mKFShader.drawIndexed(GL_LINES, 0, 8);
  }
}

void Viewer::drawGraph()
{
  auto pKFs = mpMap->GetAllKeyFrames();

  std::vector<Eigen::Vector3f> positions;
  std::vector<Eigen::Vector2i> indices;
  std::map<uint64_t, size_t> ids;

  for (KeyFrame *pKF : pKFs) {
    if (pKF->isBad())
      continue;

    cv::Mat Ow = pKF->GetCameraCenter();
    positions.push_back(Eigen::Vector3f(Ow.ptr<float>()));
    ids[pKF->mnId] = positions.size() - 1;
  }

  for (KeyFrame *pKF : pKFs) {

    auto kfid = ids[pKF->mnId];

    // Covisibility Graph
    for (KeyFrame *vit : pKF->GetCovisiblesByWeight(100)) {
      if (vit->mnId < pKF->mnId)
        indices.push_back(Eigen::Vector2i(kfid, ids[vit->mnId]));
    }

    // Spanning tree
    KeyFrame *pParent = pKF->GetParent();
    if (pParent)
      indices.push_back(Eigen::Vector2i(kfid, ids[pParent->mnId]));

    // Loops
    for (KeyFrame *pkfl : pKF->GetLoopEdges()) {
      if (pkfl->mnId < pKF->mnId)
        indices.push_back(Eigen::Vector2i(kfid, ids[pkfl->mnId]));
    }
  }

  nanogui::MatrixXu glindices(2, indices.size());
  uint32_t i = 0;
  for (auto &index : indices)
    glindices.col(i++) << index(0), index(1);

  Eigen::MatrixXf glpositions(3, positions.size());
  i = 0;
  for (auto &pos3f : positions)
    glpositions.col(i++) << pos3f;

  Eigen::Matrix4f mvp = getViewProjectionMatrix();
  mGraphShader.bind();
  mGraphShader.uploadAttrib("position", glpositions);
  mGraphShader.uploadIndices(glindices);
  mGraphShader.setUniform("mvp", mvp);
  mGraphShader.drawIndexed(GL_LINES, 0, indices.size());
}

void Viewer::requestFinish()
{
  // mainloop will finish if there is nothing to draw
  if (mbExitFinish)
    setVisible(false);
}

void Viewer::drawFrame()
{
  cv::Mat img, img2;
  std::vector<int> vMatches;              // Correspondences with reference keypoints
  std::vector<cv::KeyPoint> vIniKeys;     // KeyPoints in reference frame
  std::vector<cv::KeyPoint> vCurrentKeys; // KeyPoints in current frame
  std::vector<MapPoint *> vpMapPoints;    // MapPoints observed from the current frame
  std::vector<bool> vbVO, vbMap;          // Tracked MapPoints in current frame
  int state;                              // Tracking state

  {
    std::unique_lock<std::mutex> lock(mMutexFrame);
    state = mState;
    if (mState == Tracking::SYSTEM_NOT_READY)
      mState = Tracking::NO_IMAGES_YET;

    mCurrentFrame.copyTo(img);
    mCurrentFrame.copyTo(img2);

    vIniKeys = mvIniKeys;
    vCurrentKeys = mvCurrentKeys;
    vMatches = mvIniMatches;
    if (mState == Tracking::OK) {
      vbVO = mvbVO;
      vbMap = mvbMap;
      vpMapPoints = mvpMapPoints;
    }
  }

  // convert color space to RGB for OpenGL texture
  cv::cvtColor(img, img, img.channels() > 1 ? cv::COLOR_BGR2RGB : cv::COLOR_GRAY2RGB);

#ifdef SLAM_DEBUG
  // draw all detected ORB features
  cv::drawKeypoints(img, vCurrentKeys, img, cv::Scalar(0, 0, 255));
#endif

  // draw matchings with lines
  if (state == Tracking::NOT_INITIALIZED) {
    for (size_t i = 0; i < vMatches.size(); ++i) {
      if (vMatches[i] >= 0) {
        cv::line(img, vIniKeys[i].pt, vCurrentKeys[vMatches[i]].pt, cv::Scalar(0, 255, 0), 5);
      }
    }
  } else if (state == Tracking::OK) {
    mnTracked = 0;
    mnTrackedVO = 0;
    const float r = 5;
    for (size_t i = 0; i < vCurrentKeys.size(); i++) {
      if (vbVO[i] || vbMap[i]) {
        cv::Point2f pt1, pt2;
        pt1.x = vCurrentKeys[i].pt.x - r;
        pt1.y = vCurrentKeys[i].pt.y - r;
        pt2.x = vCurrentKeys[i].pt.x + r;
        pt2.y = vCurrentKeys[i].pt.y + r;

        //if (vMatches[i] >= 0)
        //  cv::line(img, vIniKeys[i].pt, vCurrentKeys[vMatches[i]].pt, cv::Scalar(0, 255, 0), 5);
        // Draw the trajectory over the image plane
        //        MapPoint *pMP = vpMapPoints[i];
        //        cv::KeyPoint kp2, kp1 = vCurrentKeys[i];
        //        auto vmObs = pMP->GetObservations();

        //        uint32_t count = 0;
        //        for (auto it = vmObs.rbegin(); it != vmObs.rend(); ++it) {
        //          // Draw only long-tracked points
        //          if (count > 3)
        //            break;
        //          kp2 = it->first->mvKeys[it->second];
        //          cv::line(img, kp1.pt, kp2.pt, cv::Scalar(0, 255, 0), 1);
        //          kp1 = kp2;
        //          count++;
        //        }

        // This is a match to a well-constraint MapPoint
        if (vbMap[i]) {
          cv::rectangle(img, pt1, pt2, cv::Scalar(255, 0, 0));
          cv::circle(img, vCurrentKeys[i].pt, 2, cv::Scalar(255, 0, 0), -1);
          mnTracked++;
        } else {
          // This matches to a recently inserted MapPoint
          cv::rectangle(img, pt1, pt2, cv::Scalar(0, 0, 255));
          cv::circle(img, vCurrentKeys[i].pt, 2, cv::Scalar(0, 0, 255), -1);
          mnTrackedVO++;
        }
      }
    }
  }

  // Compute and show the gradient image using Scharr 3x3 filter
  //  cv::Mat sharr_dx, sharr_dy;
  //  if (img2.channels() > 1)
  //    cv::cvtColor(img2, img2, cv::COLOR_BGR2GRAY);
  //  cv::Scharr(img2, sharr_dx, CV_16S, 1, 0);
  //  cv::Scharr(img2, sharr_dy, CV_16S, 0, 1);
  //  cv::Mat scharr = cv::abs(sharr_dx + sharr_dy);
  //  scharr.convertTo(scharr, CV_8U);
  //  cv::threshold(scharr, scharr, 200, 255, cv::THRESH_BINARY);
  //  cv::drawKeypoints(scharr, vCurrentKeys, scharr, cv::Scalar(255));
  //  cv::imshow("scharr", scharr);
  //  char k = cv::waitKey(25);
  //  if ('s' == k) {
  //    std::ostringstream ss;
  //    ss << "/tmp/" << nFID << ".jpg";
  //    std::cout << "Saved frame to " << ss.str() << std::endl;
  //    cv::imwrite(ss.str(), img);
  //  }

  // adapt aspect ratio
  cv::Rect2f roi;
  roi.width = mTexSize.width;
  roi.height = mTexSize.height * (mTexSize.width / mCurrentFrame.cols);
  roi.x = 0.0f;
  roi.y = (mTexSize.height - roi.height) / 2.0f;

  mTexture = cv::Mat(mTexSize, img.type(), cv::Scalar(0, 0, 0));
  cv::resize(img, mTexture(roi), roi.size());
  drawFrameStats(mTexture, state);
}

void Viewer::drawFrameStats(cv::Mat &im, int nState)
{
  std::stringstream s;
  if (nState == Tracking::NO_IMAGES_YET)
    s << "WAITING FOR IMAGES";
  else if (nState == Tracking::NOT_INITIALIZED)
    s << "TRYING TO INITIALIZE";
  else if (nState == Tracking::OK) {
    size_t nKFs = mpMap->KeyFramesInMap();
    size_t nMPs = mpMap->PointsCount();
    s << "Frame: " << mnFID << ", KF: " << nKFs << ", Points: " << nMPs;
    s << ", Matches: (" << mnTracked << ", " << mnTrackedVO << ")";
  } else if (nState == Tracking::LOST) {
    s << "TRACK LOST. TRYING TO RELOCALIZE";
  } else if (nState == Tracking::SYSTEM_NOT_READY) {
    s << "LOADING ORB VOCABULARY. PLEASE WAIT...";
  }

  int baseline = 0;
  cv::Size textSize = cv::getTextSize(s.str(), cv::FONT_HERSHEY_PLAIN, 1, 1, &baseline);
  textSize.height += 10; // add some padding

  im.rowRange(im.rows - textSize.height, im.rows) = cv::Mat::zeros(textSize.height, im.cols, im.type());
  cv::Point p(5, im.rows - 5);
  cv::putText(im, s.str(), p, cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(255, 255, 255), 1, 8);
}

void Viewer::updateFrame(Tracking *pTracker)
{
  std::unique_lock<std::mutex> lock(mMutexFrame);

  pTracker->mCurrentFrame.mImRGB.copyTo(mCurrentFrame);
  mbOnlyTracking = pTracker->mbOnlyTracking;
  mnFID = pTracker->mCurrentFrame.mnId;
  mvCurrentKeys = pTracker->mCurrentFrame.mvKeys;
  N = mvCurrentKeys.size();
  mvbVO = std::vector<bool>(N, false);
  mvbMap = std::vector<bool>(N, false);

  if (pTracker->mLastProcessedState == Tracking::NOT_INITIALIZED) {
    mvIniKeys = pTracker->mInitialFrame.mvKeys;
    mvIniMatches = pTracker->mvIniMatches;
  } else if (pTracker->mLastProcessedState == Tracking::OK) {
    mvIniKeys = pTracker->mLastFrame.mvKeys;
    mvIniMatches = pTracker->mCurrentFrame.mviMatchings;
    mvpMapPoints = pTracker->mCurrentFrame.mvpMapPoints;
    for (size_t i = 0; i < N; i++) {
      MapPoint *pMP = mvpMapPoints[i];
      if (pMP) {
        if (!pTracker->mCurrentFrame.mvbOutlier[i]) {
          if (pMP->Observations() > 2)
            mvbMap[i] = true;
          else
            mvbVO[i] = true;
        }
      }
    }
  }

  mState = static_cast<int>(pTracker->mLastProcessedState);
}

void Viewer::detectPlane()
{
  auto vInlierMPs = Plane::Detect(mpSystem->GetTrackedMapPoints());
  if (vInlierMPs.size() > 10) {
    cv::Mat Tcw;
    {
      std::unique_lock<std::mutex> lock(mMutexCamera);
      Tcw = mCameraPose.clone();
    }
    mPlane = new Plane(vInlierMPs, Tcw);
  } else {
    INFO_STREAM("Plane", "Not enough inliers: " << vInlierMPs.size())
  }
}

void Viewer::drawPlane()
{
  if (mPlane) {
    mPlaneShader.bind();
    mPlaneShader.setUniform("color", nanogui::Color(0.0f, 1.0f, 0.0f, 1.0f));
    cv::Mat Twc = mPlane->Tpw.t(); // OpenGL uses column-wise matrices
    Eigen::Matrix4f model(Twc.ptr<float>());
    Eigen::Matrix4f mvp = getViewProjectionMatrix() * model;
    mPlaneShader.setUniform("mvp", mvp);
    mPlaneShader.drawIndexed(GL_LINES, 0, 42);
  }
}

void Viewer::createMapShader()
{
  mMapShader.init(
      "a_map_shader",

      "#version 330\n"
      "uniform mat4 mvp;\n"
      "in vec3 position;\n"
      "in vec3 color;\n"
      "out vec3 rgb;\n"
      "void main() {\n"
      "  rgb = color;\n"
      "  gl_PointSize = 3.0;\n"
      "  gl_Position = mvp * vec4(position, 1.0);\n"
      "}",

      "#version 330\n"
      "in vec3 rgb;\n"
      "out vec4 rgba;\n"
      "void main() {\n"
      "  rgba = vec4(rgb, 1.0);\n"
      "}");
}

void Viewer::createPlaneShader()
{
  mPlaneShader.init(
      "a_plane_shader",

      "#version 330\n"
      "uniform mat4 mvp;\n"
      "in vec3 position;\n"
      "void main() {\n"
      "  gl_Position = mvp * vec4(position, 1.0);\n"
      "}",

      "#version 330\n"
      "out vec4 color;\n"
      "void main() {\n"
      "  color = vec4(vec3(0.0, 1.0, 0.0), 1.0);\n"
      "}");

  // Plane Y=0
  const int ndivs = 10;
  const int nlines = 2 * ndivs + 1;
  const float ndivsize = 0.05f;
  const float minx = -ndivs * ndivsize;
  const float minz = -ndivs * ndivsize;
  const float maxx = ndivs * ndivsize;
  const float maxz = ndivs * ndivsize;

  nanogui::MatrixXu indices(2, 2 * nlines);
  Eigen::MatrixXf positions(3, 4 * nlines);
  for (int i = 0; i < nlines; ++i) {
    float x = minx + ndivsize * i;
    float z = minz + ndivsize * i;
    positions.col(4 * i) << x, 0, minz;
    positions.col(4 * i + 1) << x, 0, maxz;
    indices.col(2 * i) << 4 * i, 4 * i + 1;
    positions.col(4 * i + 2) << minx, 0, z;
    positions.col(4 * i + 3) << maxx, 0, z;
    indices.col(2 * i + 1) << 4 * i + 2, 4 * i + 3;
  }

  mPlaneShader.bind();
  mPlaneShader.uploadAttrib("position", positions);
  mPlaneShader.uploadIndices(indices);
}

void Viewer::createKeyFrameShader()
{
  mKFShader.init(
      "a_kf_shader",

      "#version 330\n"
      "uniform mat4 mvp;\n"
      "in vec3 position;\n"
      "void main() {\n"
      "  gl_Position = mvp * vec4(position, 1.0);\n"
      "}",

      "#version 330\n"
      "uniform vec4 color;\n"
      "out vec4 rgba;\n"
      "void main() {\n"
      "  rgba = color;\n"
      "}");

  const float &w = mKeyFrameSize;
  const float h = w * 0.75f;
  const float z = w * 0.6f;

  Eigen::MatrixXf positions(3, 5);
  positions.col(0) << 0, 0, 0;
  positions.col(1) << w, h, z;
  positions.col(2) << w, -h, z;
  positions.col(3) << -w, -h, z;
  positions.col(4) << -w, h, z;

  nanogui::MatrixXu indices(2, 8);
  indices.col(0) << 0, 1;
  indices.col(1) << 0, 2;
  indices.col(2) << 0, 3;
  indices.col(3) << 0, 4;
  indices.col(4) << 1, 2;
  indices.col(5) << 4, 3;
  indices.col(6) << 4, 1;
  indices.col(7) << 3, 2;

  mKFShader.bind();
  mKFShader.uploadAttrib("position", positions);
  mKFShader.uploadIndices(indices);
}

void Viewer::createGraphShader()
{
  mGraphShader.init(
      "a_graph_shader",

      "#version 330\n"
      "uniform mat4 mvp;\n"
      "in vec3 position;\n"
      "void main() {\n"
      "  gl_Position = mvp * vec4(position, 1.0);\n"
      "}",

      "#version 330\n"
      "out vec4 color;\n"
      "void main() {\n"
      "  color = vec4(vec3(0.0, 1.0, 0.0), 1.0);\n"
      "}");
}
}
